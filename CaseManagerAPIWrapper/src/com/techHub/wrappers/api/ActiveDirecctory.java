package com.techHub.wrappers.api;

import java.util.Hashtable;
import javax.naming.ldap.*;
import javax.naming.directory.*;
import javax.naming.*;


public class ActiveDirecctory     {
     public static void main (String[] args)     {
          //set up one connection for the Global Catalog port; 3268
          //and another for the normal  LDAP port; 389
          Hashtable envGC = new Hashtable();
          Hashtable envDC = new Hashtable();

          String adminName = "CN=p8admin,CN=Users,DC=COM";
          String adminPassword = "filenet";

          //Note the GC port; 3268, and the normal LDAP port 389
          //Just for the hell of it, lets use a different GC and DC
          String urlGC = "ldap://ecmdemo1.ecm.ibm.local:3268";
          String urlDC = "ldap://ecmdemo1.ecm.ibm.local:389";

          envGC.put(Context.INITIAL_CONTEXT_FACTORY,"com.sun.jndi.ldap.LdapCtxFactory");
          envDC.put(Context.INITIAL_CONTEXT_FACTORY,"com.sun.jndi.ldap.LdapCtxFactory");

          //set security credentials, note using simple cleartext authentication
          envGC.put(Context.SECURITY_AUTHENTICATION,"simple");
          envGC.put(Context.SECURITY_PRINCIPAL,adminName);
          envGC.put(Context.SECURITY_CREDENTIALS,adminPassword);

          envDC.put(Context.SECURITY_AUTHENTICATION,"simple");
          envDC.put(Context.SECURITY_PRINCIPAL,adminName);
          envDC.put(Context.SECURITY_CREDENTIALS,adminPassword);

          //connect to both a GC and  DC
          envGC.put(Context.PROVIDER_URL,urlGC);
          envDC.put(Context.PROVIDER_URL,urlDC);
          
          //We need to chase referrals when retrieving attributes from the DC
          //as the object may be in a different domain
          envDC.put(Context.REFERRAL,"follow");
               
          try {

               //Create the initial directory context for both DC and GC
               LdapContext ctxGC = new InitialLdapContext(envGC,null);
               LdapContext ctxDC = new InitialLdapContext(envDC,null);
               
          
               //Now perform a search against the GC
               //Create the search controls           
               SearchControls searchCtls = new SearchControls();
          
               //Specify the attributes to return
               String returnedAtts[]={"sn","givenName","mail","wwwHomePage"};
               searchCtls.setReturningAttributes(returnedAtts);
          
               //Specify the search scope
               searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);

               //specify the LDAP search filter
               String searchFilter = "(objectClass=user)";

               //Specify the Base for the search
               //an empty dn for all objects from all domains in the forest
               String searchBase = "";

               //initialize counter to total the results
               int totalResults = 0;


               //Search for objects in the GC using the filter
               NamingEnumeration answer = ctxGC.search(searchBase, searchFilter, searchCtls);

               //Loop through the search results
               while (answer.hasMoreElements()) {
                    SearchResult sr = (SearchResult)answer.next();

                    totalResults++;

                    System.out.println(">>>" + sr.getName());

                    // Print out some of the attributes, catch the exception if the attributes have no values
                    Attributes attrs = sr.getAttributes();
                    if (attrs != null) {
                         try {
                              System.out.println("   name(GC): " + attrs.get("givenName").get() + " " + attrs.get("sn").get());
                              System.out.println("   mail(GC): " + attrs.get("mail").get());
                         }
                         catch (NullPointerException e)     {
                              System.err.println("Problem listing attributes from Global Catalog: " + e);
                         }
                    
                    }
                    //Now retrieve attributes from the DC
                    Attributes DCattrs = ctxDC.getAttributes(sr.getName());
                    try {
                         System.out.println("        Web(DC): " + DCattrs.get("wWWHomePage").get());
                         System.out.println("        Fax(DC): " + DCattrs.get("facsimileTelephoneNumber").get());
                    }
                    catch (NullPointerException e)     {
                         System.err.println("Problem listing attributes from Domain Controller: " + e);
                    }

               }

                System.out.println("Total results: " + totalResults);
               ctxGC.close();
               ctxDC.close();

          } 
          catch (NamingException e) {
               System.err.println("Problem searching directory: " + e);
          }

     }     
}