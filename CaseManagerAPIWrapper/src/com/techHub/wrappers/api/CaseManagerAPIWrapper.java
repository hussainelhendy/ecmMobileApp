package com.techHub.wrappers.api;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.InitialLdapContext;
import javax.naming.ldap.LdapContext;
import javax.security.auth.Subject;

import com.filenet.api.collection.ContentElementList;
import com.filenet.api.collection.FolderSet;
import com.filenet.api.constants.AutoClassify;
import com.filenet.api.constants.AutoUniqueName;
import com.filenet.api.constants.CheckinType;
import com.filenet.api.constants.DefineSecurityParentage;
import com.filenet.api.constants.RefreshMode;
import com.filenet.api.core.Connection;
import com.filenet.api.core.ContentTransfer;
import com.filenet.api.core.Document;
import com.filenet.api.core.Domain;
import com.filenet.api.core.Factory;
import com.filenet.api.core.Folder;
import com.filenet.api.core.ReferentialContainmentRelationship;
import com.filenet.api.meta.ClassDescription;
import com.filenet.api.meta.PropertyDescription;
import com.filenet.api.property.Properties;
import com.filenet.api.query.SearchSQL;
import com.filenet.api.query.SearchScope;
import com.filenet.api.util.Id;
import com.filenet.api.util.UserContext;
import com.ibm.casemgmt.api.Case;
import com.ibm.casemgmt.api.CaseMgmtObjectStore;
import com.ibm.casemgmt.api.context.CaseMgmtContext;
import com.ibm.casemgmt.api.context.P8ConnectionCache;
import com.ibm.casemgmt.api.context.SimpleP8ConnectionCache;
import com.ibm.casemgmt.api.context.SimpleVWSessionCache;
import com.ibm.casemgmt.api.exception.CaseMgmtException;
import com.ibm.casemgmt.api.objectref.DomainReference;
import com.ibm.casemgmt.api.objectref.ObjectStoreReference;
import com.ibm.casemgmt.api.properties.CaseMgmtProperty;
import com.ibm.casemgmt.api.tasks.Task;
import com.techHub.models.CaseModel;
import com.techHub.models.actionModel;
import com.techHub.models.classModel;
import com.techHub.models.filterModel;
import com.techHub.models.historyModel;
import com.techHub.models.searchfilterModel;
import com.techHub.models.taskModel;

import filenet.api.wrapper.connection.CMConnection;
import filenet.api.wrapper.objectstore.CMObjectStore;
import filenet.vw.api.VWAttachment;
import filenet.vw.api.VWFetchType;
import filenet.vw.api.VWMapDefinition;
import filenet.vw.api.VWParticipantHistory;
import filenet.vw.api.VWProcess;
import filenet.vw.api.VWQueue;
import filenet.vw.api.VWQueueQuery;
import filenet.vw.api.VWRole;
import filenet.vw.api.VWRoster;
import filenet.vw.api.VWRosterQuery;
import filenet.vw.api.VWSession;
import filenet.vw.api.VWStepElement;
import filenet.vw.api.VWStepHistory;
import filenet.vw.api.VWStepOccurrenceHistory;
import filenet.vw.api.VWStepWorkObjectHistory;
import filenet.vw.api.VWWorkBasket;
import filenet.vw.api.VWWorkObject;
import filenet.vw.api.VWWorkObjectNumber;
import filenet.vw.api.VWWorkflowDefinition;
import filenet.vw.api.VWWorkflowHistory;

public class CaseManagerAPIWrapper {
	private CaseMgmtObjectStore caseMgObjectStore;
	private CMConnection cmConnection;
	private CMObjectStore cmObjectStore;
	private static Logger log = Logger.getLogger(CaseManagerAPIWrapper.class.getSimpleName());
	private CaseMgmtContext origCmctx;
	private ObjectStoreReference objStore;
	private VWSession vwSession;
	private static P8ConnectionCache connCache;
	private static Connection conn;
	private static DomainReference domain;
	private static Domain myDomain;
	private com.filenet.api.core.EntireNetwork entireNetwork;
	private Connection connection;

	public CaseManagerAPIWrapper() {

	}

	public void close() {
		CaseMgmtContext.set(origCmctx);
		vwSession.logoff();
	}

	private void init(String objectStore, String userName, String password, String connectionPoint, String serverName,
			String loginContext) {
		objStore = new ObjectStoreReference(domain, objectStore); // $NON-NLS-1$
		Subject subject = UserContext.createSubject(conn, userName, // $NON-NLS-1$
																	// //$NON-NLS-2$
				password, loginContext); // $NON-NLS-1$ //$NON-NLS-2$
		UserContext uc = UserContext.get();
		uc.pushSubject(subject);
		origCmctx = CaseMgmtContext.set(new CaseMgmtContext(new SimpleVWSessionCache(), connCache));
		caseMgObjectStore = CaseMgmtObjectStore.fetchInstance(objStore);
		vwSession.logon(userName, // $NON-NLS-1$ //$NON-NLS-2$
				password, connectionPoint); // $NON-NLS-1$ //$NON-NLS-2$
											// //$NON-NLS-3$
		entireNetwork = Factory.EntireNetwork.fetchInstance(connection, null);
		myDomain = entireNetwork.get_LocalDomain();
	}

	public Boolean login(String objectStore, String userName, String password, String connectionPoint,
			String serverName, String loginContext, String Dmoain) {

		if (domain == null) {
			connCache = new SimpleP8ConnectionCache();
			conn = connCache.getP8Connection("http://" + serverName + "/wsi/FNCEWS40MTOM/"); //$NON-NLS-1$
			domain = new DomainReference("http://" + serverName + "/wsi/FNCEWS40MTOM/"); //$NON-NLS-1$
		}
		try {
			cmConnection = CMConnection.generateCMConnection("http://" + serverName + "/wsi/FNCEWS40MTOM/", userName, //$NON-NLS-1$ //$NON-NLS-2$
					password); // $NON-NLS-1$
			connection = Factory.Connection.getConnection("http://" + serverName + "/wsi/FNCEWS40MTOM/");
			cmObjectStore = CMObjectStore.generateCMObjectStore(cmConnection, Dmoain, objectStore); // $NON-NLS-1$
																									// //$NON-NLS-2$

			System.out.println("cmObjectStore" + cmObjectStore);
			vwSession = new VWSession();
			vwSession.setBootstrapCEURI("http://" + serverName + "/wsi/FNCEWS40MTOM/"); //$NON-NLS-1$

			init(objectStore, userName, password, connectionPoint, serverName, loginContext);
		} catch (Exception e) {
			// e.printStackTrace();
			// log.log(Level.SEVERE, "Failed to Initialize Case Manager APIs",
			// e);
			return false;
		}
		return true;
	}

	public ArrayList<taskModel> getInbox() {
		ArrayList<taskModel> tasklist = new ArrayList<>();
		String queueName = "Inbox";
		// Retrieve the Queue
		VWQueue queue = vwSession.getQueue(queueName);

		// Set Query Parameters
		// Query Flags and Type to retrieve Step Elements
		int queryFlags = VWQueue.QUERY_READ_LOCKED;
		int queryType = VWFetchType.FETCH_TYPE_STEP_ELEMENT;
		VWQueueQuery queueQuery = queue.createQuery(null, null, null, queryFlags, null, null, queryType);
		// Process Results
		while (queueQuery.hasNext()) {
			VWStepElement stepElement = (VWStepElement) queueQuery.next();
			taskModel details = new taskModel();
			details.setId(stepElement.getWorkObjectNumber());
			details.setTaskName(stepElement.getStepName());System.out.println(stepElement.getStepName());
			tasklist.add(details);
		}

		return tasklist;
	}
	
	@SuppressWarnings("deprecation")
	public ArrayList<taskModel> getInbox(String appSpaceName) {
		ArrayList<taskModel> tasklist = new ArrayList<>();
		VWRole[] vwRoles = vwSession.fetchMyRoles(appSpaceName);
		// fetch role

		for (int j = 0; j < vwRoles.length; j++) {
			VWRole vwRole = vwRoles[j];

			VWWorkBasket[] myInBaskets = vwRole.fetchWorkBaskets();
			// fetch inbasket
			for (int i = 0; i < myInBaskets.length; i++) {
				VWWorkBasket currentInBasket = myInBaskets[i];

				String queueName = currentInBasket.getQueueName();
				if (queueName.equalsIgnoreCase("Inbox")) {
					// Retrieve the Queue
					VWQueue queue = vwSession.getQueue(queueName);
					// Set Query Parameters
					// Query Flags and Type to retrieve Step Elements
					int queryFlags = VWQueue.QUERY_READ_LOCKED;
					int queryType = VWFetchType.FETCH_TYPE_STEP_ELEMENT;
					VWQueueQuery queueQuery = queue.createQuery(null, null, null, queryFlags, null, null, queryType);
					// Process Results
					while (queueQuery.hasNext()) {
						VWStepElement stepElement = (VWStepElement) queueQuery.next();
						taskModel details = new taskModel();
						details.setId(stepElement.getWorkObjectNumber());
						details.setTaskName(stepElement.getStepName());
						tasklist.add(details);
					}
				}
			}

		}

		return tasklist;
	}

	public taskModel getTaskDetails(String taskID, String AttachName) {

		// Set Roster Name
		String rosterName = Task.fetchInstance(objStore, new Id(taskID)).getRosterName();
		VWStepElement rosterItem = null;
		// Retrieve Roster Object and Roster count
		VWRoster roster = vwSession.getRoster(rosterName);
		System.out.println("Workflow Count: " + roster.fetchCount());
		// Set Query Parameters
		int queryFlags = VWRoster.QUERY_NO_OPTIONS;
		String queryFilter = "F_CaseTask=:A";
		// VWWorkObjectNumber class takes care of the value format
		// used in place of F_WobNum and F_WorkFlowNumber
		Object[] substitutionVars = { new VWWorkObjectNumber(taskID) };
		// Perform Query
		VWRosterQuery query = roster.createQuery(null, null, null, queryFlags, null, null,
				VWFetchType.FETCH_TYPE_STEP_ELEMENT);
		// Process Results
		while (query.hasNext()) {
			rosterItem = (VWStepElement) query.next();
		}
		VWAttachment vwattach = (VWAttachment) rosterItem.getParameterValue(AttachName);
		Document doc = fetchDocByID(vwattach.getId());

		taskModel model = new taskModel();
		model.setId(taskID);
		model.setTaskName(rosterItem.getStepName());
		model.setTaskAttachments(getDocumentContentAsBytes(doc));

		return model;
	}

	public taskModel getTaskDetails(String taskID) {

		// Set Roster Name
		String rosterName = Task.fetchInstance(objStore, new Id(taskID)).getRosterName();
		VWStepElement rosterItem = null;
		// Retrieve Roster Object and Roster count
		VWRoster roster = vwSession.getRoster(rosterName);
		System.out.println("Workflow Count: " + roster.fetchCount());
		// Set Query Parameters
		int queryFlags = VWRoster.QUERY_NO_OPTIONS;
		String queryFilter = "F_CaseTask=:A";
		// VWWorkObjectNumber class takes care of the value format
		// used in place of F_WobNum and F_WorkFlowNumber
		Object[] substitutionVars = { new VWWorkObjectNumber(taskID) };
		// Perform Query
		VWRosterQuery query = roster.createQuery(null, null, null, queryFlags, null, null,
				VWFetchType.FETCH_TYPE_STEP_ELEMENT);
		// Process Results
		while (query.hasNext()) {
			rosterItem = (VWStepElement) query.next();
		}
		taskModel model = new taskModel();
		model.setId(taskID);
		model.setTaskName(rosterItem.getStepName());
		
		for (String propName : rosterItem.getParameterNames()) {
			if(rosterItem.getParameterValue(propName) instanceof  VWAttachment){
				VWAttachment vwattach = (VWAttachment) rosterItem.getParameterValue(propName);
				Document doc = fetchDocByID(vwattach.getId());
				for(Entry<String, byte[]> entry : getDocumentContentAsBytes(doc).entrySet()) {
					model.getTaskAttachments().put(entry.getKey(), entry.getValue());
				}
			}
		}


		return model;
	}
	
	public HashMap<String, byte[]> getDocumentContentAsBytes(Document doc) {

		HashMap<String, byte[]> ContentList = new HashMap<String, byte[]>();
		// Print information about content elements.
		System.out.println("No. of document content elements: " + doc.get_ContentElements().size() + "\n"
				+ "Total size of content: " + doc.get_ContentSize() + "\n");

		// Get content elements and iterate list.
		ContentElementList docContentList = doc.get_ContentElements();
		Iterator iter = docContentList.iterator();
		while (iter.hasNext()) {
			ContentTransfer ct = (ContentTransfer) iter.next();

			// Print element sequence number and content type of the element.
			System.out.println("\nElement Sequence number: " + ct.get_ElementSequenceNumber().intValue() + "\n"
					+ "Content type: " + ct.get_ContentType() + "\n");

			// Get and print the content of the element.
			InputStream stream = ct.accessContentStream();
			String readStr = "";
			try {

				int n = 1;
				while (n > 0) {
					int docLen = 1024;
					byte[] buf = new byte[docLen];
					n = stream.read(buf, 0, docLen);
					readStr = readStr + new String(buf);
					buf = new byte[docLen];
					ContentList.put(ct.get_ElementSequenceNumber().toString()+n, buf);

				}
				System.out.println("Content:\n " + readStr);
				stream.close();
			} catch (IOException ioe) {
				ioe.printStackTrace();
			}
		}
		return ContentList;
	}

	public Document fetchDocByID(String docID) {
		return Factory.Document.fetchInstance(cmObjectStore.getObjectStore(), docID, null);
	}

	public ArrayList<taskModel> getOtherInbox(String appSpaceName, String Role, String inbasket) {
		ArrayList<taskModel> tasklist = new ArrayList<>();
		// fetch role
		VWRole vwRole = vwSession.fetchMyRole(Role, appSpaceName);

		VWWorkBasket[] myInBaskets = vwRole.fetchWorkBaskets();
		VWWorkBasket myInBasket = null;
		// fetch inbasket
		for (int i = 0; i < myInBaskets.length; i++) {
			VWWorkBasket currentInBasket = myInBaskets[i];
			String myInbasketName = currentInBasket.getName();
			if (myInbasketName.equals(inbasket)) {
				myInBasket = currentInBasket;
				break;
			}
		}

		String queueName = myInBasket.getQueueName();
		// Retrieve the Queue
		VWQueue queue = vwSession.getQueue(queueName);
		// Set Query Parameters
		// Query Flags and Type to retrieve Step Elements
		int queryFlags = VWQueue.QUERY_READ_LOCKED;
		int queryType = VWFetchType.FETCH_TYPE_STEP_ELEMENT;
		VWQueueQuery queueQuery = queue.createQuery(null, null, null, queryFlags, null, null, queryType);
		// Process Results
		while (queueQuery.hasNext()) {
			VWStepElement stepElement = (VWStepElement) queueQuery.next();
			taskModel details = new taskModel();
			details.setId(stepElement.getWorkObjectNumber());
			details.setTaskName(stepElement.getStepName());
			tasklist.add(details);
		}

		return tasklist;
	}

	@SuppressWarnings("deprecation")
	public ArrayList<taskModel> getOtherInbox(String appSpaceName, String Role) {
		ArrayList<taskModel> tasklist = new ArrayList<>();

		// fetch role
		VWRole vwRole = vwSession.fetchMyRole(Role, appSpaceName);

		VWWorkBasket[] myInBaskets = vwRole.fetchWorkBaskets();

		// fetch inbasket
		for (int i = 0; i < myInBaskets.length; i++) {
			VWWorkBasket currentInBasket = myInBaskets[i];

			String queueName = currentInBasket.getQueueName();
			if (!queueName.equalsIgnoreCase("Inbox")) {
				// Retrieve the Queue
				VWQueue queue = vwSession.getQueue(queueName);
				// Set Query Parameters
				// Query Flags and Type to retrieve Step Elements
				int queryFlags = VWQueue.QUERY_READ_LOCKED;
				int queryType = VWFetchType.FETCH_TYPE_STEP_ELEMENT;
				VWQueueQuery queueQuery = queue.createQuery(null, null, null, queryFlags, null, null, queryType);
				// Process Results
				while (queueQuery.hasNext()) {
					VWStepElement stepElement = (VWStepElement) queueQuery.next();
					taskModel details = new taskModel();
					details.setId(stepElement.getWorkObjectNumber());
					details.setTaskName(stepElement.getStepName());
					tasklist.add(details);
				}
			}

		}

		return tasklist;
	}

	@SuppressWarnings("deprecation")
	public ArrayList<taskModel> getOtherInbox(String appSpaceName) {
		ArrayList<taskModel> tasklist = new ArrayList<>();
		VWRole[] vwRoles = vwSession.fetchMyRoles(appSpaceName);
		// fetch role

		for (int j = 0; j < vwRoles.length; j++) {
			VWRole vwRole = vwRoles[j];

			VWWorkBasket[] myInBaskets = vwRole.fetchWorkBaskets();
			// fetch inbasket
			for (int i = 0; i < myInBaskets.length; i++) {
				VWWorkBasket currentInBasket = myInBaskets[i];

				String queueName = currentInBasket.getQueueName();
				if (!queueName.equalsIgnoreCase("Inbox")) {
					// Retrieve the Queue
					VWQueue queue = vwSession.getQueue(queueName);
					// Set Query Parameters
					// Query Flags and Type to retrieve Step Elements
					int queryFlags = VWQueue.QUERY_READ_LOCKED;
					int queryType = VWFetchType.FETCH_TYPE_STEP_ELEMENT;
					VWQueueQuery queueQuery = queue.createQuery(null, null, null, queryFlags, null, null, queryType);
					// Process Results
					while (queueQuery.hasNext()) {
						VWStepElement stepElement = (VWStepElement) queueQuery.next();
						taskModel details = new taskModel();
						details.setId(stepElement.getWorkObjectNumber());
						details.setTaskName(stepElement.getStepName());
						tasklist.add(details);
					}
				}
			}

		}

		return tasklist;
	}

	public ArrayList<taskModel> searchInbox(String Property, String KeyWord) {
		ArrayList<taskModel> tasklist = new ArrayList<>();
		String queueName = "Inbox";
		// Retrieve the Queue
		VWQueue queue = vwSession.getQueue(queueName);
		// Set Query Parameters
		// Query Flags and Type to retrieve Step Elements
		int queryFlags = VWQueue.QUERY_READ_LOCKED;
		int queryType = VWFetchType.FETCH_TYPE_QUEUE_ELEMENT;
		String filter = Property + " like '" + KeyWord+"%'";

		VWQueueQuery queueQuery = queue.createQuery(null, null, null, queryFlags, filter, null, queryType);
		// Process Results
		while (queueQuery.hasNext()) {
			VWStepElement stepElement = (VWStepElement) queueQuery.next();
			taskModel details = new taskModel();
			details.setId(stepElement.getWorkObjectNumber());
			details.setTaskName(stepElement.getStepName());
			tasklist.add(details);
		}

		return tasklist;
	}

	public ArrayList<taskModel> searchInboxWithFilter(List<searchfilterModel> filterItems) {
		ArrayList<taskModel> tasklist = new ArrayList<>();
		String queueName = "Inbox";
		// Retrieve the Queue
		VWQueue queue = vwSession.getQueue(queueName);
		// Set Query Parameters
		// Query Flags and Type to retrieve Step Elements
		int queryFlags = VWQueue.QUERY_READ_LOCKED;
		int queryType = VWFetchType.FETCH_TYPE_QUEUE_ELEMENT;
		String filter = "";
		for (int i = 0; i < filterItems.size(); i++) {
			filter = filterItems.get(i).getPropertyName() + " like '" + filterItems.get(i).getPropertyValue()  +"%'";
			if (i < filterItems.size() - 1)
				filter += "AND ";
		}

		VWQueueQuery queueQuery = queue.createQuery(null, null, null, queryFlags, filter, null, queryType);
		// Process Results
		while (queueQuery.hasNext()) {
			VWStepElement stepElement = (VWStepElement) queueQuery.next();
			taskModel details = new taskModel();
			details.setId(stepElement.getWorkObjectNumber());
			details.setTaskName(stepElement.getStepName());
			tasklist.add(details);
		}

		return tasklist;
	}

	public CaseModel getCase(String caseID) {
		CaseModel caseModel = null;
		try {

			Case cmCase = Case.fetchInstance(objStore, new Id(caseID), null, null);
			if (cmCase != null){
				caseModel = new CaseModel();
				if(cmCase.getCaseTitle() != null)
				caseModel.setCaseName(cmCase.getCaseTitle());
				System.out.println(cmCase.getCaseTitle().toString()+ " :::: ");
				caseModel.setCaseID(caseID);
				cmCase.getProperties().asList();
				for (CaseMgmtProperty element : cmCase.getProperties().asList()) {
					System.out.println(element.getDisplayName() +" : "+ element.getValue());
					caseModel.getPramaters().put(element.getDisplayName(),  element.getValue() !=null ? element.getValue().toString(): null);
				}
			}
		} catch (CaseMgmtException e) {
			log.log(Level.SEVERE, "Case Manager General Exception", e);
		}
		return caseModel;
	}

	public CaseModel getCaseUsingTaskID(String taskID) {
		Task task = getTaskByID(taskID);
		CaseModel caseModel = null;
		try {

			String caseID = task.getCaseFolderId().toString();
			Case cmCase = Case.fetchInstance(objStore, new Id(caseID), null, null);
			if (cmCase != null){
				caseModel = new CaseModel();
				if(cmCase.getCaseTitle() != null)
				caseModel.setCaseName(cmCase.getCaseTitle());
				System.out.println(cmCase.getCaseTitle().toString()+ " :::: ");
				caseModel.setCaseID(caseID);
				cmCase.getProperties().asList();
				for (CaseMgmtProperty element : cmCase.getProperties().asList()) {
					System.out.println(element.getDisplayName() +" : "+ element.getValue());
					caseModel.getPramaters().put(element.getDisplayName(),  element.getValue() !=null ? element.getValue().toString(): null);
				}
			}
			
		} catch (CaseMgmtException e) {
			log.log(Level.SEVERE, "Case Manager General Exception", e);
		}
		return caseModel;
	}

	public Task getTaskByID(String taskID) {
		return Task.fetchInstance(objStore, new Id(taskID));
	}

	public CaseModel getCases(String classType) {

		try {
			String sqlQuery = "SELECT * FROM " + classType;
			SearchSQL searchSQL = new SearchSQL(sqlQuery);
			SearchScope searchScope = new SearchScope(cmObjectStore.getObjectStore());
			FolderSet folderSet = (FolderSet) searchScope.fetchObjects(searchSQL, null, null, true);

			Iterator<Folder> folders = folderSet.iterator();
			while (folders.hasNext()) {
				System.out.println(folders.next().get_FolderName());
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}

	public void reassign(String taskID, String participant) {

		// Set Roster Name
		String rosterName = Task.fetchInstance(objStore, new Id(taskID)).getRosterName();
		// Retrieve Roster Object and Roster count
		VWRoster roster = vwSession.getRoster(rosterName);
		System.out.println("Workflow Count: " + roster.fetchCount());
		// Set Query Parameters
		int queryFlags = VWRoster.QUERY_NO_OPTIONS;
		String queryFilter = "F_CaseTask=:A";
		// VWWorkObjectNumber class takes care of the value format
		// used in place of F_WobNum and F_WorkFlowNumber
		Object[] substitutionVars = { new VWWorkObjectNumber(taskID) };
		int fetchType = VWFetchType.FETCH_TYPE_ROSTER_ELEMENT;
		// Perform Query
		VWRosterQuery query = roster.createQuery(null, null, null, queryFlags, null, null,
				VWFetchType.FETCH_TYPE_STEP_ELEMENT);
		// Process Results
		while (query.hasNext()) {
			VWStepElement rosterItem = (VWStepElement) query.next();
			rosterItem.doReassign(participant, true, null);
		}

	}

	public void completeTask(String taskID) {
		// Set Roster Name
		String rosterName = Task.fetchInstance(objStore, new Id(taskID)).getRosterName();
		VWStepElement rosterItem = null;
		// Retrieve Roster Object and Roster count
		VWRoster roster = vwSession.getRoster(rosterName);
		System.out.println("Workflow Count: " + roster.fetchCount());
		// Set Query Parameters
		int queryFlags = VWRoster.QUERY_NO_OPTIONS;
		String queryFilter = "F_CaseTask=:A";
		// VWWorkObjectNumber class takes care of the value format
		// used in place of F_WobNum and F_WorkFlowNumber
		Object[] substitutionVars = { new VWWorkObjectNumber(taskID) };
		// Perform Query
		VWRosterQuery query = roster.createQuery(null, null, null, queryFlags, null, null,
				VWFetchType.FETCH_TYPE_STEP_ELEMENT);
		// Process Results
		while (query.hasNext()) {
			rosterItem = (VWStepElement) query.next();
		}
		rosterItem.doLock(true);
		rosterItem.doDispatch();

	}

	public List<String> getAvaliableUsers(String adminName, String adminPassword, String urlGC) {

		List<String> users = new ArrayList<String>();
		// set up one connection for the Global Catalog port; 3268
		// and another for the normal LDAP port; 389
		Hashtable envGC = new Hashtable();
		envGC.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");

		// set security credentials, note using simple cleartext authentication
		envGC.put(Context.SECURITY_AUTHENTICATION, "simple");
		envGC.put(Context.SECURITY_PRINCIPAL, adminName);
		envGC.put(Context.SECURITY_CREDENTIALS, adminPassword);

		// //connect to both a GC and DC
		envGC.put(Context.PROVIDER_URL, urlGC);

		try {

			// Create the initial directory context for both DC and GC
			LdapContext ctxGC = new InitialLdapContext(envGC, null);

			// Now perform a search against the GC
			// Create the search controls
			SearchControls searchCtls = new SearchControls();

			// Specify the attributes to return
			String returnedAtts[] = { "sn", "givenName", "mail", "wwwHomePage" };
			searchCtls.setReturningAttributes(returnedAtts);

			// Specify the search scope
			searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);

			// specify the LDAP search filter
			String searchFilter = "(objectClass=user)";

			// Specify the Base for the search
			// an empty dn for all objects from all domains in the forest
			String searchBase = "";

			// initialize counter to total the results
			int totalResults = 0;

			// Search for objects in the GC using the filter
			NamingEnumeration answer = ctxGC.search(searchBase, searchFilter, searchCtls);

			// Loop through the search results
			while (answer.hasMoreElements()) {
				SearchResult sr = (SearchResult) answer.next();

				totalResults++;

				System.out.println(">>>" + sr.getName());
				users.add(sr.getName());

				// Print out some of the attributes, catch the exception if the
				// attributes have no values
				/*
				 * Attributes attrs = sr.getAttributes(); if (attrs != null) {
				 * try { System.out.println("   name(GC): " +
				 * attrs.get("givenName").get() + " " + attrs.get("sn").get());
				 * System.out.println("   mail(GC): " +
				 * attrs.get("mail").get()); } catch (NullPointerException e) {
				 * System.err.
				 * println("Problem listing attributes from Global Catalog: " +
				 * e); }
				 * 
				 * }
				 */

			}

			System.out.println("Total results: " + totalResults);
			ctxGC.close();

		} catch (NamingException e) {
			System.err.println("Problem searching directory: " + e);
		}

		return users;
	}

	public ArrayList<filterModel> geFiltertList(String appSpaceName, String Role) {

		ArrayList<filterModel> finalFilters = new ArrayList<filterModel>();
		ArrayList<taskModel> tasklist = new ArrayList<>();
		// fetch role
		VWRole vwRole = vwSession.fetchMyRole(Role, appSpaceName);

		VWWorkBasket[] myInBaskets = vwRole.fetchWorkBaskets();
		VWWorkBasket myInBasket = null;
		// fetch inbasket
		for (int i = 0; i < myInBaskets.length; i++) {
			VWWorkBasket.Filter[] filters = myInBaskets[i].getFilters();
			if (filters != null) {
				for (VWWorkBasket.Filter filter : filters) {
					filterModel filterModel = new filterModel(filter.getName(), String.valueOf(filter.getOperator()),
							filter.getDescription());
					finalFilters.add(filterModel);

				}

			}

		}

		return finalFilters;
	}

	public historyModel getTaskHistory(String taskID) {

		historyModel historyModelVariable = null;
		// Set Roster Name
		String rosterName = Task.fetchInstance(objStore, new Id(taskID)).getRosterName();
		// Retrieve Roster Object and Roster count
		VWRoster roster = vwSession.getRoster(rosterName);
		System.out.println("Workflow Count: " + roster.fetchCount());
		// Set Query Parameters
		int queryFlags = VWRoster.QUERY_NO_OPTIONS;
		String queryFilter = "F_CaseTask=:A";
		// VWWorkObjectNumber class takes care of the value format
		// used in place of F_WobNum and F_WorkFlowNumber
		Object[] substitutionVars = { new VWWorkObjectNumber(taskID) };
		int fetchType = VWFetchType.FETCH_TYPE_ROSTER_ELEMENT;
		// Perform Query
		VWRosterQuery query = roster.createQuery(null, null, null, queryFlags, null, null,
				VWFetchType.FETCH_TYPE_WORKOBJECT);
		// Process Results
		while (query.hasNext()) {
			VWWorkObject rosterItem = (VWWorkObject) query.next();
			VWProcess process = rosterItem.fetchProcess();
			VWWorkflowDefinition workflowDefinition = process.fetchWorkflowDefinition(false);
			VWMapDefinition[] maps = workflowDefinition.getMaps();
			for (int i = 0; i < maps.length; i++) {
				System.out.println("Map Name: " + maps[i].getName());
				int mapID = maps[i].getMapId();
				System.out.println("Map Id: " + mapID);
				VWWorkflowHistory workflowHistory = process.fetchWorkflowHistory(mapID);
				System.out.println("Originator: " + workflowHistory.getOriginator());
				System.out.println("<stepHistory Info>");
				while (workflowHistory.hasNext()) {
					VWStepHistory stepHistory = workflowHistory.next();
					System.out.println(stepHistory.getStepName());
					System.out.println("\t<stepOccurenceHistory Info>");
					while (stepHistory.hasNext()) {
						VWStepOccurrenceHistory stepOccurenceHistory = stepHistory.next();
						System.out.println(
								"\tDate received: " + stepOccurenceHistory.getDateReceived() + "</Datereceived>");
						System.out.println(
								"\tDate completed: " + stepOccurenceHistory.getCompletionDate() + "</Date completed>");
						System.out.println("\t\t<stepWorkObjectHistory Info>");
						historyModelVariable = new historyModel(stepHistory.getStepName(),
								stepOccurenceHistory.getDateReceived().toString(),
								stepOccurenceHistory.getCompletionDate().toString());
						while (stepOccurenceHistory.hasNext()) {
							VWStepWorkObjectHistory stepWorkObjectHistory = stepOccurenceHistory.next();
							System.out.println("\t\t\t<ParticipantHistory Info>");
							while (stepWorkObjectHistory.hasNext()) {
								VWParticipantHistory participantHistory = stepWorkObjectHistory.next();
								System.out.println("\t\t\tDate received = " + participantHistory.getDateReceived()
										+ "</Date received>");
								System.out.println(
										"\t\t\tComments = " + participantHistory.getComments() + "</Comments>");
								System.out.println("\t\t\tUser = " + participantHistory.getUserName() + "</User>");
								System.out.println("\t\t\tParticipant = " + participantHistory.getParticipantName()
										+ "</Participant>");
								participantHistory.getCompletionDate();
								participantHistory.getOperationName();

								historyModelVariable.getActions()
										.add(new actionModel(participantHistory.getUserName(),
												participantHistory.getDateReceived().toString(),
												participantHistory.getComments(),
												participantHistory.getCompletionDate().toString(),
												participantHistory.getOperationName()));

							} // while stepWorkObjectHistory
							System.out.println("\t\t\t</ParticipantHistory Info>");
						} // while stepOccurenceHistory
						System.out.println("\t\t</stepWorkObjectHistory Info>");
					} // while stepHistory
					System.out.println("\t</stepOccurenceHistoryInfo>");
				} // while workflowHistory
			} // for Workflow maps
		} // while rosterQuery hasnext() Retrieve

		return historyModelVariable;
	}

	public void dispatchTaskSteps(Task cmTask, String stepName, String response) {
		VWRoster roster = vwSession.getRoster(cmTask.getRosterName());
		int queryFlags = VWRoster.QUERY_NO_OPTIONS;
		String queryFilter = "F_CaseTask=:A"; //$NON-NLS-1$
		int queryType = VWFetchType.FETCH_TYPE_STEP_ELEMENT;
		Object[] substitutionVars = { new VWWorkObjectNumber(cmTask.getId().toString()) };
		VWRosterQuery query = roster.createQuery(null, null, null, queryFlags, queryFilter, substitutionVars,
				queryType);

		while (query.hasNext()) {
			VWStepElement stepElement = (VWStepElement) query.next();
			if (!stepElement.getProperty("F_StepName").equals(stepName)) { //$NON-NLS-1$
				continue;
			}
			if (response != null) {
				stepElement.setSelectedResponse(response);
			}
			stepElement.doLock(true);
			stepElement.doDispatch();
			
		}

	}

	@SuppressWarnings("unchecked")
	public void uploadDoc(String CaseID, String className, String contentName, String memeType, byte[] docContent,
			HashMap<String, String> prop) {
		Document document = Factory.Document.createInstance(cmObjectStore.getObjectStore(), className);
		Case cmCase = getCaseUsingId(CaseID);
		Folder folderDestination = cmCase.getFolderReference().fetchCEObject();

		ContentTransfer content = Factory.ContentTransfer.createInstance();
		content.set_RetrievalName(contentName);
		content.setCaptureSource(new ByteArrayInputStream(docContent));
		content.set_ContentType(memeType);
		ContentElementList contentElementList = Factory.ContentElement.createList();
		contentElementList.add(content);
		document.set_ContentElements(contentElementList);

		// Check-in the doc
		document.checkin(AutoClassify.DO_NOT_AUTO_CLASSIFY, CheckinType.MAJOR_VERSION);
		// Get and put the doc properties
		String documentName = className;
		Properties p = document.getProperties();

		for (Map.Entry<String, String> entry : prop.entrySet()) {
			String key = entry.getKey();
			String value = entry.getValue();
			p.putValue(key, value);
		}
		p.putValue("DocumentTitle", documentName);
		document.save(RefreshMode.REFRESH);

		if (folderDestination != null) {
			ReferentialContainmentRelationship referentialContainmentRelationship = folderDestination.file(document,
					AutoUniqueName.NOT_AUTO_UNIQUE, document.get_Name(),
					DefineSecurityParentage.DO_NOT_DEFINE_SECURITY_PARENTAGE);
			referentialContainmentRelationship.save(RefreshMode.REFRESH);
		}
	}

	private Case getCaseUsingId(String caseID) {
		try {

			Case cmCase = Case.fetchInstanceFromIdentifier(objStore, caseID);

			if (cmCase == null) {
				log.info("Case Not Found");

			}
			return cmCase;
		} catch (CaseMgmtException e) {
			e.printStackTrace();
			log.log(Level.SEVERE, "Case Manager General Exception", e);

		}
		return null;
	}

	public List<classModel> getClasses() {

		List<classModel> classesList = new ArrayList<>();
		Iterator x = cmObjectStore.getObjectStore().get_ClassDescriptions().iterator();

		classModel Model = new classModel();
		while (x.hasNext()) {
			ClassDescription objClassDesc = (ClassDescription) x.next();
			Model.setCalssSymbloicName(objClassDesc.get_SymbolicName());
			Model.setCalssID(objClassDesc.get_Id().toString());
			Iterator y = objClassDesc.get_PropertyDescriptions().iterator();
			while (y.hasNext())
				Model.getMetaData().add(((PropertyDescription) y.next()).get_SymbolicName());
			classesList.add(Model);
		}
		return classesList;
	}
}