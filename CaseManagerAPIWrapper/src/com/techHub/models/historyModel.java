package com.techHub.models;

import java.util.ArrayList;
import java.util.List;

public class historyModel {
	
	private String StepName;
	private String DateReceived;
	private String CompletionDate;
	List<actionModel> actions = new ArrayList<actionModel>();
	
	public historyModel(String stepName, String dateReceived, String completionDate) {
		super();
		StepName = stepName;
		DateReceived = dateReceived;
		CompletionDate = completionDate;
	}

	public String getStepName() {
		return StepName;
	}

	public void setStepName(String stepName) {
		StepName = stepName;
	}

	public String getDateReceived() {
		return DateReceived;
	}

	public void setDateReceived(String dateReceived) {
		DateReceived = dateReceived;
	}

	public String getCompletionDate() {
		return CompletionDate;
	}

	public void setCompletionDate(String completionDate) {
		CompletionDate = completionDate;
	}

	public List<actionModel> getActions() {
		return actions;
	}

	public void setActions(List<actionModel> actions) {
		this.actions = actions;
	}
	

	

}
