package com.techHub.models;

import java.util.ArrayList;
import java.util.List;

public class classModel {

	
	private String CalssID;
	private String CalssSymbloicName;
	private List<String> metaData = new ArrayList<>();

	public classModel(){
		
	}

	public String getCalssID() {
		return CalssID;
	}

	public void setCalssID(String calssID) {
		CalssID = calssID;
	}

	public String getCalssSymbloicName() {
		return CalssSymbloicName;
	}

	public void setCalssSymbloicName(String calssSymbloicName) {
		CalssSymbloicName = calssSymbloicName;
	}

	public List<String> getMetaData() {
		return metaData;
	}

	public void setMetaData(List<String> metaData) {
		this.metaData = metaData;
	}
	
	
	
}
