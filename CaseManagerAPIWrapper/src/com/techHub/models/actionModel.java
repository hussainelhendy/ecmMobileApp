package com.techHub.models;

public class actionModel {
	private String userName;
	private String DateReceived;
	private String Comments;
	private String CompletionDate;
	private String OperationName;
	public actionModel(String userName, String dateReceived, String comments, String completionDate,
			String operationName) {
		super();
		this.userName = userName;
		DateReceived = dateReceived;
		Comments = comments;
		CompletionDate = completionDate;
		OperationName = operationName;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getDateReceived() {
		return DateReceived;
	}
	public void setDateReceived(String dateReceived) {
		DateReceived = dateReceived;
	}
	public String getComments() {
		return Comments;
	}
	public void setComments(String comments) {
		Comments = comments;
	}
	public String getCompletionDate() {
		return CompletionDate;
	}
	public void setCompletionDate(String completionDate) {
		CompletionDate = completionDate;
	}
	public String getOperationName() {
		return OperationName;
	}
	public void setOperationName(String operationName) {
		OperationName = operationName;
	}
	
	

}
