package com.techHub.models;

public class filterModel {
	
	private String filterName;
	private String filterDescription;
	private String filterOperator;

	
	public filterModel(String propertyName, String propertyValue, String filterOperator) {
		this.filterName = propertyName;
		this.filterDescription = propertyValue;
		this.filterOperator = filterOperator;
	}
	
	public String getPropertyName() {
		return filterName;
	}
	public void setPropertyName(String propertyName) {
		this.filterName = propertyName;
	}
	public String getPropertyValue() {
		return filterDescription;
	}
	public void setPropertyValue(String propertyValue) {
		this.filterDescription = propertyValue;
	}

	public String getFilterOperator() {
		return filterOperator;
	}

	public void setFilterOperator(String filterOperator) {
		this.filterOperator = filterOperator;
	}
	
	

}
