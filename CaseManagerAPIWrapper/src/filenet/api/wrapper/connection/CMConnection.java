package filenet.api.wrapper.connection;

import javax.security.auth.Subject;

import com.filenet.api.core.Connection;
import com.filenet.api.core.Factory;
import com.filenet.api.util.UserContext;

/**
 * @author techHup
 * 
 */
public class CMConnection {
	private Connection connection;
	private String uri;
	private String userName;
	private String userPassword;

	public static CMConnection generateCMConnection(String uri,
			String userName, String userPassword) throws Exception {
		return new CMConnection(uri, userName, userPassword);
	}

	private CMConnection(String uri, String userName, String userPassword)
			throws Exception {
		this.setUserPassword(userPassword);
		this.setUserName(userName);
		this.setUri(uri);
		this.setConnection();
	}

	private void setConnection() throws Exception {
		Connection connection = Factory.Connection.getConnection(this.getUri());
		UserContext userContext = UserContext.get();
		Subject subject = UserContext.createSubject(connection,
				this.getUserName(), this.getUserPassword(), "FileNetP8WSI");
		userContext.pushSubject(subject);
		this.connection = connection;
	}

	private void setUri(String uri) {
		this.uri = uri;
	}

	private void setUserName(String userName) {
		this.userName = userName;
	}

	private void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}

	public Connection getConnection() {
		return this.connection;
	}

	public String getUri() {
		return this.uri;
	}

	public String getUserName() {
		return this.userName;
	}

	public String getUserPassword() {
		return this.userPassword;
	}

}
