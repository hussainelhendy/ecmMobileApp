package filenet.api.wrapper.objectstore;

import com.filenet.api.core.Domain;
import com.filenet.api.core.Factory;
import com.filenet.api.core.ObjectStore;

import filenet.api.wrapper.bundlekeys.ErrorBundleKeys;
import filenet.api.wrapper.connection.CMConnection;

/**
 * @author TechHup
 * 
 */
public class CMObjectStore {
	private ObjectStore objectStore;
	private CMConnection cmConnection;
	private String domainName;
	private String objectStoreName;

	public static CMObjectStore generateCMObjectStore(
			CMConnection cmConnection, String domainName, String objectStoreName)
			throws Exception {
		return new CMObjectStore(cmConnection, domainName, objectStoreName);
	}

	private CMObjectStore(CMConnection cmConnection, String domainName,
			String objectStoreName) throws Exception {
		this.setObjectStoreName(objectStoreName);
		this.setDomainName(domainName);
		this.setCMConnection(cmConnection);
		this.setObjectStore();
	}

	private void setObjectStore() throws Exception {
		Domain domain = Factory.Domain.fetchInstance(this.getCMConnection()
				.getConnection(), this.getDomainName(), null);
		ObjectStore objectStore = Factory.ObjectStore.fetchInstance(domain,
				this.getObjectStoreName(), null);
		if (objectStore == null)
			throw new Exception(ErrorBundleKeys.CMOBJECTSTORENOTFOUND_STRING);
		this.objectStore = objectStore;
	}

	private void setCMConnection(CMConnection cmConnection) {
		this.cmConnection = cmConnection;
	}

	private void setDomainName(String domainName) {
		this.domainName = domainName;
	}

	private void setObjectStoreName(String objectStoreName) {
		this.objectStoreName = objectStoreName;
	}

	public ObjectStore getObjectStore() {
		return this.objectStore;
	}

	public CMConnection getCMConnection() {
		return this.cmConnection;
	}

	public String getDomainName() {
		return this.domainName;
	}

	public String getObjectStoreName() {
		return this.objectStoreName;
	}

}
