package filenet.api.wrapper.bundlekeys;

/**
 * @author techHup
 * 
 */
public class ErrorBundleKeys {
	public static final String CMCONNECTIONSINGLEINSTANCE_STRING = "CMConnection is already created. Can not create except one instance.";
	public static final String CMOBJECTSTORESINGLEINSTANCE_STRING = "CMObjectStore is already created. Can not create except one instance.";
	public static final String CMOBJECTSTORENOTFOUND_STRING = "CMObjectStore is not found.";
	public static final String ATTRIBUTES_STRING = "Attribute has already been set.";
	public static final String QUERY_STRING = "Can not search with this type of attribute.";
	public static final String ATTRIBUTEMISSING_STRING = "Attribute is not set.";
	public static final String ATTRIBUTETYPE_STRING = "This attribute type is not set.";
}
