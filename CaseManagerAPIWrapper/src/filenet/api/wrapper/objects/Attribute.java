package filenet.api.wrapper.objects;

import java.io.InputStream;
import java.util.Date;

import com.filenet.api.collection.BinaryList;
import com.filenet.api.collection.BooleanList;
import com.filenet.api.collection.DateTimeList;
import com.filenet.api.collection.DependentObjectList;
import com.filenet.api.collection.Float64List;
import com.filenet.api.collection.IdList;
import com.filenet.api.collection.IndependentObjectSet;
import com.filenet.api.collection.Integer32List;
import com.filenet.api.collection.StringList;
import com.filenet.api.core.EngineObject;
import com.filenet.api.util.Id;

import filenet.api.wrapper.bundlekeys.ErrorBundleKeys;
import javax.xml.bind.annotation.*;
/**
 * @author techHup
 * 
 */
@XmlRootElement(name = "attribute")
public class Attribute {
	@XmlElement
	private String name;
	@XmlElement
	private String type = null;
	private Object objectValue;
	private BinaryList binaryListValue;
	@XmlElement
	private boolean booleanValueb;
	private Boolean booleanValueB;
	private BooleanList booleanListValue;
	private byte[] byteValue;
	@XmlElement
	private Date dateValue;
	private DateTimeList dateTimeListValue;
	private DependentObjectList dependentObjectListValue;
	@XmlElement
	private double doubleValued;
	private Double doubleValueD;
	private EngineObject engineObjectValue;
	private Float64List float64ListValue;
	private Id iDValue;
	private IdList idListValue;
	private IndependentObjectSet independentObjectSetValue;
	private InputStream inputStreamValue;
	private int intValue;
	private Integer integerValue;
	private Integer32List integer32ListValue;
	@XmlElement
	private String stringValue;
	private StringList stringListValue;

	
	public Attribute()
	{
		
	}
	public Attribute(String name, Object objectValue) throws Exception {
		super();
		if (this.getType() != null)
			throw new Exception(ErrorBundleKeys.ATTRIBUTES_STRING);
		this.setName(name);
		this.setType("Object");
		this.setObjectValue(objectValue);
	}

	public Attribute(String name, BinaryList binaryListValue) throws Exception {
		super();
		if (this.getType() != null)
			throw new Exception(ErrorBundleKeys.ATTRIBUTES_STRING);
		this.setName(name);
		this.setType("BinaryList");
		this.setBinaryListValue(binaryListValue);
	}

	public Attribute(String name, boolean booleanValueb) throws Exception {
		super();
		if (this.getType() != null)
			throw new Exception(ErrorBundleKeys.ATTRIBUTES_STRING);
		this.setName(name);
		this.setType("boolean");
		this.setBooleanValueb(booleanValueb);
	}

	public Attribute(String name, Boolean booleanValueB) throws Exception {
		super();
		if (this.getType() != null)
			throw new Exception(ErrorBundleKeys.ATTRIBUTES_STRING);
		this.setName(name);
		this.setType("Boolean");
		this.setBooleanValueB(booleanValueB);
	}

	public Attribute(String name, BooleanList booleanListValue)
			throws Exception {
		super();
		if (this.getType() != null)
			throw new Exception(ErrorBundleKeys.ATTRIBUTES_STRING);
		this.setName(name);
		this.setType("BooleanList");
		this.setBooleanListValue(booleanListValue);
	}

	public Attribute(String name, byte[] byteValue) throws Exception {
		super();
		if (this.getType() != null)
			throw new Exception(ErrorBundleKeys.ATTRIBUTES_STRING);
		this.setName(name);
		this.setType("byte[]");
		this.setByteValue(byteValue);
	}

	public Attribute(String name, Date dateValue) throws Exception {
		super();
		if (this.getType() != null)
			throw new Exception(ErrorBundleKeys.ATTRIBUTES_STRING);
		this.setName(name);
		this.setType("Date");
		this.setDateValue(dateValue);
	}

	public Attribute(String name, DateTimeList dateTimeListValue)
			throws Exception {
		super();
		if (this.getType() != null)
			throw new Exception(ErrorBundleKeys.ATTRIBUTES_STRING);
		this.setName(name);
		this.setType("DateTimeList");
		this.setDateTimeListValue(dateTimeListValue);
	}

	public Attribute(String name, DependentObjectList dependentObjectListValue)
			throws Exception {
		super();
		if (this.getType() != null)
			throw new Exception(ErrorBundleKeys.ATTRIBUTES_STRING);
		this.setName(name);
		this.setType("DependentObjectList");
		this.setDependentObjectListValue(dependentObjectListValue);
	}

	public Attribute(String name, double doubleValued) throws Exception {
		super();
		if (this.getType() != null)
			throw new Exception(ErrorBundleKeys.ATTRIBUTES_STRING);
		this.setName(name);
		this.setType("double");
		this.setDoubleValued(doubleValued);
	}

	public Attribute(String name, Double doubleValueD) throws Exception {
		super();
		if (this.getType() != null)
			throw new Exception(ErrorBundleKeys.ATTRIBUTES_STRING);
		this.setName(name);
		this.setType("Double");
		this.setDoubleValueD(doubleValueD);
	}

	public Attribute(String name, EngineObject engineObjectValue)
			throws Exception {
		super();
		if (this.getType() != null)
			throw new Exception(ErrorBundleKeys.ATTRIBUTES_STRING);
		this.setName(name);
		this.setType("EngineObject");
		this.setEngineObjectValue(engineObjectValue);
	}

	public Attribute(String name, Float64List float64ListValue)
			throws Exception {
		super();
		if (this.getType() != null)
			throw new Exception(ErrorBundleKeys.ATTRIBUTES_STRING);
		this.setName(name);
		this.setType("Float64List");
		this.setFloat64ListValue(float64ListValue);
	}

	public Attribute(String name, Id iDValue) throws Exception {
		super();
		if (this.getType() != null)
			throw new Exception(ErrorBundleKeys.ATTRIBUTES_STRING);
		this.setName(name);
		this.setType("Id");
		this.setiDValue(iDValue);
	}

	public Attribute(String name, IdList idListValue) throws Exception {
		super();
		if (this.getType() != null)
			throw new Exception(ErrorBundleKeys.ATTRIBUTES_STRING);
		this.setName(name);
		this.setType("IdList");
		this.setIdListValue(idListValue);
	}

	public Attribute(String name, IndependentObjectSet independentObjectSetValue)
			throws Exception {
		super();
		if (this.getType() != null)
			throw new Exception(ErrorBundleKeys.ATTRIBUTES_STRING);
		this.setName(name);
		this.setType("IndependentObjectSet");
		this.setIndependentObjectSetValue(independentObjectSetValue);
	}

	public Attribute(String name, InputStream inputStreamValue)
			throws Exception {
		super();
		if (this.getType() != null)
			throw new Exception(ErrorBundleKeys.ATTRIBUTES_STRING);
		this.setName(name);
		this.setType("InputStream");
		this.setInputStreamValue(inputStreamValue);
	}

	public Attribute(String name, int intValue) throws Exception {
		super();
		if (this.getType() != null)
			throw new Exception(ErrorBundleKeys.ATTRIBUTES_STRING);
		this.setName(name);
		this.setType("int");
		this.setIntValue(intValue);
	}

	public Attribute(String name, Integer integerValue) throws Exception {
		super();
		if (this.getType() != null)
			throw new Exception(ErrorBundleKeys.ATTRIBUTES_STRING);
		this.setName(name);
		this.setType("Integer");
		this.setIntegerValue(integerValue);
	}

	public Attribute(String name, Integer32List integer32ListValue)
			throws Exception {
		super();
		if (this.getType() != null)
			throw new Exception(ErrorBundleKeys.ATTRIBUTES_STRING);
		this.setName(name);
		this.setType("Integer32List");
		this.setInteger32ListValue(integer32ListValue);
	}
  
	public Attribute(String name, String stringValue) throws Exception {
		super();
		if (this.getType() != null)
			throw new Exception(ErrorBundleKeys.ATTRIBUTES_STRING);
		this.setName(name);
		this.setType("String");
		this.setStringValue(stringValue);
	}

	public Attribute(String name, StringList stringListValue) throws Exception {
		super();
		if (this.getType() != null)
			throw new Exception(ErrorBundleKeys.ATTRIBUTES_STRING);
		this.setName(name);
		this.setType("StringList");
		this.setStringListValue(stringListValue);
	}
	@XmlTransient
	public void setName(String name) {
		this.name = name;
	}
	@XmlTransient
	private void setType(String type) {
		this.type = type;
	}

	private void setObjectValue(Object objectValue) {
		this.objectValue = objectValue;
	}

	private void setBinaryListValue(BinaryList binaryListValue) {
		this.binaryListValue = binaryListValue;
	}
	@XmlTransient
	private void setBooleanValueb(boolean booleanValueb) {
		this.booleanValueb = booleanValueb;
	}

	private void setBooleanValueB(Boolean booleanValueB) {
		this.booleanValueB = booleanValueB;
	}

	private void setBooleanListValue(BooleanList booleanListValue) {
		this.booleanListValue = booleanListValue;
	}

	private void setByteValue(byte[] byteValue) {
		this.byteValue = byteValue;
	}
	@XmlTransient
	private void setDateValue(Date dateValue) {
		this.dateValue = dateValue;
	}

	private void setDateTimeListValue(DateTimeList dateTimeListValue) {
		this.dateTimeListValue = dateTimeListValue;
	}

	private void setDependentObjectListValue(
			DependentObjectList dependentObjectListValue) {
		this.dependentObjectListValue = dependentObjectListValue;
	}
	@XmlTransient
	private void setDoubleValued(double doubleValued) {
		this.doubleValued = doubleValued;
	}

	private void setDoubleValueD(Double doubleValueD) {
		this.doubleValueD = doubleValueD;
	}

	private void setEngineObjectValue(EngineObject engineObjectValue) {
		this.engineObjectValue = engineObjectValue;
	}

	private void setFloat64ListValue(Float64List float64ListValue) {
		this.float64ListValue = float64ListValue;
	}

	private void setiDValue(Id iDValue) {
		this.iDValue = iDValue;
	}

	private void setIdListValue(IdList idListValue) {
		this.idListValue = idListValue;
	}

	private void setIndependentObjectSetValue(
			IndependentObjectSet independentObjectSetValue) {
		this.independentObjectSetValue = independentObjectSetValue;
	}

	private void setInputStreamValue(InputStream inputStreamValue) {
		this.inputStreamValue = inputStreamValue;
	}

	private void setIntValue(int intValue) {
		this.intValue = intValue;
	}

	private void setIntegerValue(Integer integerValue) {
		this.integerValue = integerValue;
	}

	private void setInteger32ListValue(Integer32List integer32ListValue) {
		this.integer32ListValue = integer32ListValue;
	}
	@XmlTransient
	private void setStringValue(String stringValue) {
		this.stringValue = stringValue;
	}

	private void setStringListValue(StringList stringListValue) {
		this.stringListValue = stringListValue;
	}

	public String getName() {
		return name;
	}

	public String getType() {
		return type;
	}

	public Object getObjectValue() throws Exception {
		if (!this.getType().equals("Object"))
			throw new Exception(ErrorBundleKeys.ATTRIBUTETYPE_STRING);
		return objectValue;
	}

	public BinaryList getBinaryListValue() throws Exception {
		if (!this.getType().equals("BinaryList"))
			throw new Exception(ErrorBundleKeys.ATTRIBUTETYPE_STRING);
		return binaryListValue;
	}

	public boolean getBooleanValueb() throws Exception {
		if (!this.getType().equals("boolean"))
			throw new Exception(ErrorBundleKeys.ATTRIBUTETYPE_STRING);
		return booleanValueb;
	}

	public Boolean getBooleanValueB() throws Exception {
		if (!this.getType().equals("Boolean"))
			throw new Exception(ErrorBundleKeys.ATTRIBUTETYPE_STRING);
		return booleanValueB;
	}

	public BooleanList getBooleanListValue() throws Exception {
		if (!this.getType().equals("BooleanList"))
			throw new Exception(ErrorBundleKeys.ATTRIBUTETYPE_STRING);
		return booleanListValue;
	}

	public byte[] getByteValue() throws Exception {
		if (!this.getType().equals("byte[]"))
			throw new Exception(ErrorBundleKeys.ATTRIBUTETYPE_STRING);
		return byteValue;
	}

	public Date getDateValue() throws Exception {
		if (!this.getType().equals("Date"))
			throw new Exception(ErrorBundleKeys.ATTRIBUTETYPE_STRING);
		return dateValue;
	}

	public DateTimeList getDateTimeListValue() throws Exception {
		if (!this.getType().equals("DateTimeList"))
			throw new Exception(ErrorBundleKeys.ATTRIBUTETYPE_STRING);
		return dateTimeListValue;
	}

	public DependentObjectList getDependentObjectListValue() throws Exception {
		if (!this.getType().equals("DependentObjectList"))
			throw new Exception(ErrorBundleKeys.ATTRIBUTETYPE_STRING);
		return dependentObjectListValue;
	}

	public double getDoubleValued() throws Exception {
		if (!this.getType().equals("double"))
			throw new Exception(ErrorBundleKeys.ATTRIBUTETYPE_STRING);
		return doubleValued;
	}

	public Double getDoubleValueD() throws Exception {
		if (!this.getType().equals("Double"))
			throw new Exception(ErrorBundleKeys.ATTRIBUTETYPE_STRING);
		return doubleValueD;
	}

	public EngineObject getEngineObjectValue() throws Exception {
		if (!this.getType().equals("EngineObject"))
			throw new Exception(ErrorBundleKeys.ATTRIBUTETYPE_STRING);
		return engineObjectValue;
	}

	public Float64List getFloat64ListValue() throws Exception {
		if (!this.getType().equals("Float64List"))
			throw new Exception(ErrorBundleKeys.ATTRIBUTETYPE_STRING);
		return float64ListValue;
	}

	public Id getiDValue() throws Exception {
		if (!this.getType().equals("Id"))
			throw new Exception(ErrorBundleKeys.ATTRIBUTETYPE_STRING);
		return iDValue;
	}

	public IdList getIdListValue() throws Exception {
		if (!this.getType().equals("IdList"))
			throw new Exception(ErrorBundleKeys.ATTRIBUTETYPE_STRING);
		return idListValue;
	}

	public IndependentObjectSet getIndependentObjectSetValue() throws Exception {
		if (!this.getType().equals("IndependentObjectSet"))
			throw new Exception(ErrorBundleKeys.ATTRIBUTETYPE_STRING);
		return independentObjectSetValue;
	}

	public InputStream getInputStreamValue() throws Exception {
		if (!this.getType().equals("InputStream"))
			throw new Exception(ErrorBundleKeys.ATTRIBUTETYPE_STRING);
		return inputStreamValue;
	}

	public int getIntValue() throws Exception {
		if (!this.getType().equals("int"))
			throw new Exception(ErrorBundleKeys.ATTRIBUTETYPE_STRING);
		return intValue;
	}

	public Integer getIntegerValue() throws Exception {
		if (!this.getType().equals("Integer"))
			throw new Exception(ErrorBundleKeys.ATTRIBUTETYPE_STRING);
		return integerValue;
	}

	public Integer32List getInteger32ListValue() throws Exception {
		if (!this.getType().equals("Integer32List"))
			throw new Exception(ErrorBundleKeys.ATTRIBUTETYPE_STRING);
		return integer32ListValue;
	}

	public String getStringValue() throws Exception {
		if (!this.getType().equals("String"))
			throw new Exception(ErrorBundleKeys.ATTRIBUTETYPE_STRING);
		return stringValue;
	}

	public StringList getStringListValue() throws Exception {
		if (!this.getType().equals("StringList"))
			throw new Exception(ErrorBundleKeys.ATTRIBUTETYPE_STRING);
		return stringListValue;
	}
}