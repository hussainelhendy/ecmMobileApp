package filenet.api.wrapper.operations;

import com.filenet.api.constants.RefreshMode;
import com.filenet.api.constants.ReservationType;
import com.filenet.api.core.Document;
import com.filenet.api.core.Folder;

import filenet.api.wrapper.bundlekeys.ErrorBundleKeys;
import filenet.api.wrapper.objects.Attribute;

public class UpdateObject {
	public static Document updateDocument(Document document,
			Attribute[] documentAttributes, RefreshMode documentRefreshMode)
			throws Exception {
		for (int i = 0; i < documentAttributes.length; i++)
			UpdateObject.setAttribute(document, documentAttributes[i]);
		document.save(documentRefreshMode);
		return document;
	}
	public static Document updateDocumentVersion(Document document,
			Attribute[] documentAttributes, RefreshMode documentRefreshMode)
			throws Exception {
	
		for (int i = 0; i < documentAttributes.length; i++)
			UpdateObject.setAttribute(document, documentAttributes[i]);
		document.save(documentRefreshMode);
		return document;
	}
	public static Folder updateDocument(Folder folder,
			Attribute[] folderAttributes, RefreshMode folderRefreshMode)
			throws Exception {
		for (int i = 0; i < folderAttributes.length; i++)
			UpdateObject.setAttribute(folder, folderAttributes[i]);
		folder.save(folderRefreshMode);
		return folder;
	}

	public static void setAttribute(Document document, Attribute attribute)
			throws Exception {
		if (attribute.getType().equals("Object"))
			document.getProperties().putObjectValue(attribute.getName(),
					attribute.getObjectValue());
		else if (attribute.getType().equals("BinaryList"))
			document.getProperties().putValue(attribute.getName(),
					attribute.getBinaryListValue());
		else if (attribute.getType().equals("boolean"))
			document.getProperties().putValue(attribute.getName(),
					attribute.getBooleanValueb());
		else if (attribute.getType().equals("Boolean"))
			document.getProperties().putValue(attribute.getName(),
					attribute.getBooleanValueB());
		else if (attribute.getType().equals("BooleanList"))
			document.getProperties().putValue(attribute.getName(),
					attribute.getBooleanListValue());
		else if (attribute.getType().equals("byte[]"))
			document.getProperties().putValue(attribute.getName(),
					attribute.getByteValue());
		else if (attribute.getType().equals("Date"))
			document.getProperties().putValue(attribute.getName(),
					attribute.getDateValue());
		else if (attribute.getType().equals("DateTimeList"))
			document.getProperties().putValue(attribute.getName(),
					attribute.getDateTimeListValue());
		else if (attribute.getType().equals("DependentObject"))
			document.getProperties().putValue(attribute.getName(),
					attribute.getDependentObjectListValue());
		else if (attribute.getType().equals("double"))
			document.getProperties().putValue(attribute.getName(),
					attribute.getDoubleValued());
		else if (attribute.getType().equals("Double"))
			document.getProperties().putValue(attribute.getName(),
					attribute.getDoubleValueD());
		else if (attribute.getType().equals("EngineObject"))
			document.getProperties().putValue(attribute.getName(),
					attribute.getEngineObjectValue());
		else if (attribute.getType().equals("Float64List"))
			document.getProperties().putValue(attribute.getName(),
					attribute.getFloat64ListValue());
		else if (attribute.getType().equals("Id"))
			document.getProperties().putValue(attribute.getName(),
					attribute.getiDValue());
		else if (attribute.getType().equals("IdList"))
			document.getProperties().putValue(attribute.getName(),
					attribute.getIdListValue());
		else if (attribute.getType().equals("IndependentObjectSet"))
			document.getProperties().putValue(attribute.getName(),
					attribute.getIndependentObjectSetValue());
		else if (attribute.getType().equals("InputStream"))
			document.getProperties().putValue(attribute.getName(),
					attribute.getInputStreamValue());
		else if (attribute.getType().equals("int"))
			document.getProperties().putValue(attribute.getName(),
					attribute.getIntValue());
		else if (attribute.getType().equals("Integer"))
			document.getProperties().putValue(attribute.getName(),
					attribute.getIntegerValue());
		else if (attribute.getType().equals("Integer32List"))
			document.getProperties().putValue(attribute.getName(),
					attribute.getInteger32ListValue());
		else if (attribute.getType().equals("String"))
			document.getProperties().putValue(attribute.getName(),
					attribute.getStringValue());
		else if (attribute.getType().equals("StringList"))
			document.getProperties().putValue(attribute.getName(),
					attribute.getStringListValue());
		else
			throw new Exception(ErrorBundleKeys.ATTRIBUTEMISSING_STRING);
	}

	private static void setAttribute(Folder folder, Attribute attribute)
			throws Exception {
		if (attribute.getType().equals("Object"))
			folder.getProperties().putObjectValue(attribute.getName(),
					attribute.getObjectValue());
		else if (attribute.getType().equals("BinaryList"))
			folder.getProperties().putValue(attribute.getName(),
					attribute.getBinaryListValue());
		else if (attribute.getType().equals("boolean"))
			folder.getProperties().putValue(attribute.getName(),
					attribute.getBooleanValueb());
		else if (attribute.getType().equals("Boolean"))
			folder.getProperties().putValue(attribute.getName(),
					attribute.getBooleanValueB());
		else if (attribute.getType().equals("BooleanList"))
			folder.getProperties().putValue(attribute.getName(),
					attribute.getBooleanListValue());
		else if (attribute.getType().equals("byte[]"))
			folder.getProperties().putValue(attribute.getName(),
					attribute.getByteValue());
		else if (attribute.getType().equals("Date"))
			folder.getProperties().putValue(attribute.getName(),
					attribute.getDateValue());
		else if (attribute.getType().equals("DateTimeList"))
			folder.getProperties().putValue(attribute.getName(),
					attribute.getDateTimeListValue());
		else if (attribute.getType().equals("DependentObject"))
			folder.getProperties().putValue(attribute.getName(),
					attribute.getDependentObjectListValue());
		else if (attribute.getType().equals("double"))
			folder.getProperties().putValue(attribute.getName(),
					attribute.getDoubleValued());
		else if (attribute.getType().equals("Double"))
			folder.getProperties().putValue(attribute.getName(),
					attribute.getDoubleValueD());
		else if (attribute.getType().equals("EngineObject"))
			folder.getProperties().putValue(attribute.getName(),
					attribute.getEngineObjectValue());
		else if (attribute.getType().equals("Float64List"))
			folder.getProperties().putValue(attribute.getName(),
					attribute.getFloat64ListValue());
		else if (attribute.getType().equals("Id"))
			folder.getProperties().putValue(attribute.getName(),
					attribute.getiDValue());
		else if (attribute.getType().equals("IdList"))
			folder.getProperties().putValue(attribute.getName(),
					attribute.getIdListValue());
		else if (attribute.getType().equals("IndependentObjectSet"))
			folder.getProperties().putValue(attribute.getName(),
					attribute.getIndependentObjectSetValue());
		else if (attribute.getType().equals("InputStream"))
			folder.getProperties().putValue(attribute.getName(),
					attribute.getInputStreamValue());
		else if (attribute.getType().equals("int"))
			folder.getProperties().putValue(attribute.getName(),
					attribute.getIntValue());
		else if (attribute.getType().equals("Integer"))
			folder.getProperties().putValue(attribute.getName(),
					attribute.getIntegerValue());
		else if (attribute.getType().equals("Integer32List"))
			folder.getProperties().putValue(attribute.getName(),
					attribute.getInteger32ListValue());
		else if (attribute.getType().equals("String"))
			folder.getProperties().putValue(attribute.getName(),
					attribute.getStringValue());
		else if (attribute.getType().equals("StringList"))
			folder.getProperties().putValue(attribute.getName(),
					attribute.getStringListValue());
		else
			throw new Exception(ErrorBundleKeys.ATTRIBUTEMISSING_STRING);
	}
}
