/**
 * 
 */
package filenet.api.wrapper.operations;

import java.util.Iterator;

import com.filenet.api.core.Folder;
import com.filenet.api.property.Property;

import filenet.api.wrapper.connection.CMConnection;
import filenet.api.wrapper.objects.Attribute;
import filenet.api.wrapper.objectstore.CMObjectStore;

/**
 * @author Abdelsamad
 *
 */
public class TestFile {

	public static void main(String[]args) throws Exception{
		CMConnection cmConnection = CMConnection.generateCMConnection(
				"http://localhost:9080/wsi/FNCEWS40MTOM/", "administrator",
				"P@ssw0rd");
		CMObjectStore cmObjectStore = CMObjectStore.generateCMObjectStore(cmConnection,
				"P852Domain", "CMTOS");
		
		Iterator<Folder> caseFolders = FetchObject.fetchFolder(
				cmObjectStore, "CmAcmCaseFolder", new Attribute[] { new Attribute(
						"DCS_BatchID", "20150504.000005") }, FetchObject.SQLCLAUSE.AND);

		Folder caseFolder = caseFolders.next();
		Property property = caseFolder.getProperties().get(
				"CmAcmCaseIdentifier");
		String caseId = property.getStringValue();
		System.out.println(caseId);
	}
}
