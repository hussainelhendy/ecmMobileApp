package filenet.api.wrapper.operations;

import com.filenet.api.constants.RefreshMode;
import com.filenet.api.core.Document;
import com.filenet.api.core.Folder;

public class DeleteObject {
	public static Document deleteDocument(Document document, RefreshMode documentRefreshMode) throws Exception {
		document.delete();
		document.save(documentRefreshMode);
		return document;
	}

	public static Folder deleteFolder(Folder folder, RefreshMode folderRefreshMode) throws Exception {
		folder.delete();
		folder.save(folderRefreshMode);
		return folder;
	}
}
