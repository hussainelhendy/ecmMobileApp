package filenet.api.wrapper.operations;

import com.filenet.api.constants.AutoUniqueName;
import com.filenet.api.constants.DefineSecurityParentage;
import com.filenet.api.constants.RefreshMode;
import com.filenet.api.core.Document;
import com.filenet.api.core.Factory;
import com.filenet.api.core.Folder;
import com.filenet.api.core.ReferentialContainmentRelationship;

import filenet.api.wrapper.objectstore.CMObjectStore;

public class FileUnfileDocument {
	public static Document fileDocument(CMObjectStore cmObjectStore,
			Folder folderDestination, Document document,
			AutoUniqueName documentAutoUniqueName,
			DefineSecurityParentage documentDefineSecurityParentage,
			RefreshMode documentRefreshMode) throws Exception {
		if (folderDestination == null)
			folderDestination = Factory.Folder.getInstance(
					cmObjectStore.getObjectStore(), null, "/");
		ReferentialContainmentRelationship referentialContainmentRelationship = folderDestination
				.file(document, documentAutoUniqueName, document.get_Name(),
						documentDefineSecurityParentage);
		referentialContainmentRelationship.save(documentRefreshMode);
		return document;
	}

	public static Document unfileDocument(CMObjectStore cmObjectStore,
			Folder folderSource, Document document,
			RefreshMode documentRefreshMode) throws Exception {
		if (folderSource == null)
			folderSource = Factory.Folder.getInstance(
					cmObjectStore.getObjectStore(), null, "/");
		ReferentialContainmentRelationship referentialContainmentRelationship = folderSource
				.unfile(document);
		referentialContainmentRelationship.save(documentRefreshMode);
		return document;
	}

	public static Document moveDocument(CMObjectStore cmObjectStore,
			Folder folderSource, Folder folderDestination, Document document,
			AutoUniqueName documentAutoUniqueName,
			DefineSecurityParentage documentDefineSecurityParentage,
			RefreshMode documentRefreshMode) throws Exception {
		document = unfileDocument(cmObjectStore, folderSource, document,
				documentRefreshMode);
		document = fileDocument(cmObjectStore, folderDestination, document,
				documentAutoUniqueName, documentDefineSecurityParentage,
				documentRefreshMode);
		return document;
	}
}
