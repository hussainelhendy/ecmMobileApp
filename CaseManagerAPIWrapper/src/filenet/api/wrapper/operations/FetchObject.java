package filenet.api.wrapper.operations;

import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;

import com.filenet.api.collection.ClassDefinitionSet;
import com.filenet.api.collection.DocumentSet;
import com.filenet.api.collection.EngineCollection;
import com.filenet.api.collection.FolderSet;
import com.filenet.api.collection.IndependentObjectSet;
import com.filenet.api.collection.PageIterator;
import com.filenet.api.core.Document;
import com.filenet.api.core.Factory;
import com.filenet.api.core.Folder;
import com.filenet.api.query.SearchSQL;
import com.filenet.api.query.SearchScope;
import com.filenet.api.util.Id;

import filenet.api.wrapper.bundlekeys.ErrorBundleKeys;
import filenet.api.wrapper.objects.Attribute;
import filenet.api.wrapper.objectstore.CMObjectStore;

public class FetchObject {
	public static enum SQLCLAUSE {
		AND, OR
	};

	public static Iterator<Folder> fetchFolder(CMObjectStore cmObjectStore,
			String folderClass, Attribute[] folderAttributes,
			SQLCLAUSE sqlClause) throws Exception {
		String sqlQuery = generateQuery(folderClass, folderAttributes,
				sqlClause);
		SearchSQL searchSQL = new SearchSQL(sqlQuery);
		SearchScope searchScope = new SearchScope(
				cmObjectStore.getObjectStore());
		FolderSet folderSet = (FolderSet) searchScope.fetchObjects(searchSQL,
				null, null, true);
		@SuppressWarnings("unchecked")
		Iterator<Folder> foldersIterator = folderSet.iterator();
		return foldersIterator;
	}

	public static Iterator<Folder> fetchFolder(CMObjectStore cmObjectStore,
			String sqlQuery) throws Exception {
		SearchSQL searchSQL = new SearchSQL(sqlQuery);
		SearchScope searchScope = new SearchScope(
				cmObjectStore.getObjectStore());
		FolderSet folderSet = (FolderSet) searchScope.fetchObjects(searchSQL,
				null, null, true);
		@SuppressWarnings("unchecked")
		Iterator<Folder> foldersIterator = folderSet.iterator();
		return foldersIterator;
	}

	public static Iterator<Document> fetchDocument(CMObjectStore cmObjectStore,
			String documentClass, Attribute[] documentAttributes,
			SQLCLAUSE sqlClause) throws Exception {
		String sqlQuery = generateQuery(documentClass, documentAttributes,
				sqlClause);
		SearchSQL searchSQL = new SearchSQL(sqlQuery);
		SearchScope searchScope = new SearchScope(
				cmObjectStore.getObjectStore());
		DocumentSet documentsSet = (DocumentSet) searchScope.fetchObjects(
				searchSQL, null, null, true);
		@SuppressWarnings("unchecked")
		Iterator<Document> documentsIterator = documentsSet.iterator();
		return documentsIterator;
	}

	public static Iterator<Document> fetchDocument(CMObjectStore cmObjectStore,
			String sqlQuery) throws Exception {
		SearchSQL searchSQL = new SearchSQL(sqlQuery);
		SearchScope searchScope = new SearchScope(
				cmObjectStore.getObjectStore());
		DocumentSet documentsSet = (DocumentSet) searchScope.fetchObjects(
				searchSQL, null, null, true);
		@SuppressWarnings("unchecked")
		Iterator<Document> documentsIterator = documentsSet.iterator();
		return documentsIterator;
	}

	public static Iterator<?> fetchObject(CMObjectStore cmObjectStore,
			String objectClass, Attribute[] objectAttributes,
			SQLCLAUSE sqlClause) throws Exception {
		String sqlQuery = generateQuery(objectClass, objectAttributes,
				sqlClause);
		SearchSQL searchSQL = new SearchSQL(sqlQuery);
		SearchScope searchScope = new SearchScope(
				cmObjectStore.getObjectStore());
		EngineCollection engineCollection = (ClassDefinitionSet) searchScope
				.fetchObjects(searchSQL, null, null, true);
		Iterator<?> objectsIterator = engineCollection.iterator();
		return objectsIterator;
	}

	public static Iterator<?> fetchObject(CMObjectStore cmObjectStore,
			String sqlQuery) throws Exception {
		SearchSQL searchSQL = new SearchSQL(sqlQuery);
		SearchScope searchScope = new SearchScope(
				cmObjectStore.getObjectStore());
		EngineCollection engineCollection = searchScope.fetchObjects(searchSQL,
				null, null, true);
		Iterator<?> objectsIterator = engineCollection.iterator();
		return objectsIterator;
	}

	public static PageIterator fetchObjectPagination(
			CMObjectStore cmObjectStore, String objectClass,
			Attribute[] objectAttributes, SQLCLAUSE sqlClause) throws Exception {
		String sqlQuery = generateQuery(objectClass, objectAttributes,
				sqlClause);
		SearchSQL searchSQL = new SearchSQL(sqlQuery);
		SearchScope searchScope = new SearchScope(
				cmObjectStore.getObjectStore());
		IndependentObjectSet independentObjectSet = searchScope.fetchObjects(
				searchSQL, 50, null, true);
		PageIterator objectsPageIterator = independentObjectSet.pageIterator();
		return objectsPageIterator;
	}

	public static PageIterator fetchObjectPagination(
			CMObjectStore cmObjectStore, String sqlQuery) throws Exception {
		SearchSQL searchSQL = new SearchSQL(sqlQuery);
		SearchScope searchScope = new SearchScope(
				cmObjectStore.getObjectStore());
		IndependentObjectSet independentObjectSet = searchScope.fetchObjects(
				searchSQL, 50, null, true);
		PageIterator objectsPageIterator = independentObjectSet.pageIterator();
		return objectsPageIterator;
	}

	@SuppressWarnings("deprecation")
	private static String generateQuery(String objectClass,
			Attribute[] attributes, SQLCLAUSE sqlClause) throws Exception {
		String sqlQuery = "SELECT * FROM " + objectClass;
		for (int i = 0; i < attributes.length; i++) {
			if (i == 0)
				sqlQuery += " WHERE";
			if (attributes[i].getType().equals("boolean"))
				sqlQuery += " " + attributes[i].getName() + " = " + ""
						+ String.valueOf(attributes[i].getBooleanValueb())
						+ " " + String.valueOf(sqlClause);
			else if (attributes[i].getType().equals("Boolean"))
				sqlQuery += " " + attributes[i].getName() + " = "
						+ attributes[i].getBooleanValueB() + " "
						+ String.valueOf(sqlClause);
			else if (attributes[i].getType().equals("Date")) {
				Calendar calendar = Calendar.getInstance();
				calendar.setTime(attributes[i].getDateValue());
				Date beginDate = attributes[i].getDateValue();
				calendar.add(Calendar.DATE, 1);
				Date lastDate = calendar.getTime();
				sqlQuery += " " + attributes[i].getName() + " >= "
						+ beginDate.getYear()
						+ String.format("%02d", beginDate.getMonth())
						+ String.format("%02d", beginDate.getDate()) + "T"
						+ String.format("%02d", beginDate.getHours())
						+ String.format("%02d", beginDate.getMinutes())
						+ String.format("%02d", beginDate.getSeconds()) + "Z "
						+ String.valueOf(sqlClause) + " "
						+ attributes[i].getName() + " < " + lastDate.getYear()
						+ String.format("%02d", lastDate.getMonth())
						+ String.format("%02d", lastDate.getDate()) + "T"
						+ String.format("%02d", lastDate.getHours())
						+ String.format("%02d", lastDate.getMinutes())
						+ String.format("%02d", lastDate.getSeconds()) + "Z "
						+ String.valueOf(sqlClause);
			} else if (attributes[i].getType().equals("double"))
				sqlQuery += " " + attributes[i].getName() + " = "
						+ String.valueOf(attributes[i].getDoubleValued()) + " "
						+ String.valueOf(sqlClause);
			else if (attributes[i].getType().equals("Double"))
				sqlQuery += " " + attributes[i].getName() + " = "
						+ attributes[i].getDoubleValueD().toString() + " "
						+ String.valueOf(sqlClause);
			else if (attributes[i].getType().equals("Id"))
				sqlQuery += " " + attributes[i].getName() + " = "
						+ attributes[i].getiDValue().toString() + " "
						+ String.valueOf(sqlClause);
			else if (attributes[i].getType().equals("int"))
				sqlQuery += " " + attributes[i].getName() + " = "
						+ String.valueOf(attributes[i].getIntValue()) + " "
						+ String.valueOf(sqlClause);
			else if (attributes[i].getType().equals("Integer"))
				sqlQuery += " " + attributes[i].getName() + " = "
						+ attributes[i].getIntegerValue().toString() + " "
						+ String.valueOf(sqlClause);
			else if (attributes[i].getType().equals("String"))
				sqlQuery += " " + attributes[i].getName() + " = " + "'"
						+ attributes[i].getStringValue() + "' "
						+ String.valueOf(sqlClause);
			else
				throw new Exception(ErrorBundleKeys.QUERY_STRING);
		}
		sqlQuery = sqlQuery
				.substring(
						0,
						sqlQuery.length()
								- (sqlQuery.contains(String.valueOf(sqlClause)) ? String
										.valueOf(sqlClause).length() + 1 : 0));
		return sqlQuery;
	}
	
	public static Document fetchDocumentByGUID(CMObjectStore cmObjectStore, String Guid){		
		return Factory.Document.fetchInstance(cmObjectStore.getObjectStore(), new Id(Guid), null);
	}
}