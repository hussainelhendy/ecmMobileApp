package filenet.api.wrapper.operations;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;

import com.filenet.api.collection.ContentElementList;
import com.filenet.api.constants.AutoClassify;
import com.filenet.api.constants.AutoUniqueName;
import com.filenet.api.constants.CheckinType;
import com.filenet.api.constants.DefineSecurityParentage;
import com.filenet.api.constants.RefreshMode;
import com.filenet.api.core.ContentTransfer;
import com.filenet.api.core.Document;
import com.filenet.api.core.Factory;
import com.filenet.api.core.Folder;
import com.filenet.api.core.ReferentialContainmentRelationship;

import filenet.api.wrapper.bundlekeys.ErrorBundleKeys;
import filenet.api.wrapper.objects.Attribute;
import filenet.api.wrapper.objectstore.CMObjectStore;

public class CreateObject {
	@SuppressWarnings("unchecked")
	public static Document createDocumentFromBytes(CMObjectStore cmObjectStore,
			Folder folderDestination, byte[] fileBytes,String fileName,
			Attribute[] documentAttributes, String documentClass, AutoClassify documentAutoClassify,
			CheckinType documentCheckinType,
			AutoUniqueName documentAutoUniqueName,
			DefineSecurityParentage documentDefineSecurityParentage,
			RefreshMode documentRefreshMode)throws Exception {
		Document document = Factory.Document.createInstance(cmObjectStore.getObjectStore(), documentClass);
		if (fileBytes != null && fileBytes.length>0) {
			String contentType = getContentType(fileName);
			ContentTransfer contentTransfer = Factory.ContentTransfer.createInstance();
			
			ByteArrayInputStream inputStream = new ByteArrayInputStream(fileBytes);
			contentTransfer.setCaptureSource(inputStream);
			contentTransfer.set_RetrievalName(fileName);
			contentTransfer.set_ContentType(contentType);
			ContentElementList contentElementList = Factory.ContentElement
					.createList();
			contentElementList.add(contentTransfer);
			document.set_MimeType(contentType);
			document.set_ContentElements(contentElementList);
			return addDocumentToFileNet( document, documentAttributes,documentCheckinType,
					 documentAutoUniqueName, folderDestination,
					 documentDefineSecurityParentage,
					 documentRefreshMode,  documentAutoClassify) ;
		}
		else{
			throw new Exception("No Bytes sent to create docuement");
		}
		
		
	}

	public static Document addDocumentToFileNet(Document document,Attribute[] documentAttributes,
			CheckinType documentCheckinType,
			AutoUniqueName documentAutoUniqueName,Folder folderDestination,
			DefineSecurityParentage documentDefineSecurityParentage,
			RefreshMode documentRefreshMode, AutoClassify documentAutoClassify) throws Exception{
		for (int i = 0; i < documentAttributes.length; i++){
			CreateObject.setAttribute(document, documentAttributes[i]);
		}
		document.checkin(documentAutoClassify, documentCheckinType);
		document.save(documentRefreshMode);
		if (folderDestination != null) {
			ReferentialContainmentRelationship referentialContainmentRelationship = folderDestination
					.file(document, documentAutoUniqueName,
							document.get_Name(),
							documentDefineSecurityParentage);
			referentialContainmentRelationship.save(documentRefreshMode);
		}
		return document;
	}
	@SuppressWarnings("unchecked")
	public static Document createDocument(CMObjectStore cmObjectStore,
			Folder folderDestination, File documentFile, String documentClass,
			Attribute[] documentAttributes, AutoClassify documentAutoClassify,
			CheckinType documentCheckinType,
			AutoUniqueName documentAutoUniqueName,
			DefineSecurityParentage documentDefineSecurityParentage,
			RefreshMode documentRefreshMode) throws Exception {
		Document document = Factory.Document.createInstance(
				cmObjectStore.getObjectStore(), documentClass);
		if (documentFile != null) {
			String contentType = getContentType(documentFile.getName());
			ContentTransfer contentTransfer = Factory.ContentTransfer
					.createInstance();
			FileInputStream fileInputStream = new FileInputStream(documentFile);
			byte[] byteFile = new byte[(int) documentFile.length()];
			fileInputStream.read(byteFile);
			fileInputStream.close();
			ByteArrayInputStream inputStream = new ByteArrayInputStream(
					byteFile);
			contentTransfer.setCaptureSource(inputStream);
			contentTransfer.set_RetrievalName(documentFile.getName());
			contentTransfer.set_ContentType(contentType);
			ContentElementList contentElementList = Factory.ContentElement
					.createList();
			contentElementList.add(contentTransfer);
			document.set_MimeType(contentType);
			document.set_ContentElements(contentElementList);
		}
		for (int i = 0; i < documentAttributes.length; i++)
			CreateObject.setAttribute(document, documentAttributes[i]);
		document.checkin(documentAutoClassify, documentCheckinType);
		document.save(documentRefreshMode);
		if (folderDestination != null) {
			ReferentialContainmentRelationship referentialContainmentRelationship = folderDestination
					.file(document, documentAutoUniqueName,
							document.get_Name(),
							documentDefineSecurityParentage);
			referentialContainmentRelationship.save(documentRefreshMode);
		}
		return document;
	}

	public static Folder createFolder(CMObjectStore cmObjectStore,
			Folder folderDestination, String folderClass,
			Attribute[] folderAttributes, RefreshMode folderRefreshMode)
			throws Exception {
		if (folderDestination == null)
			folderDestination = Factory.Folder.getInstance(
					cmObjectStore.getObjectStore(), null, "/");
		Folder folder = Factory.Folder.createInstance(
				cmObjectStore.getObjectStore(), folderClass);
		folder.set_Parent(folderDestination);
		for (int i = 0; i < folderAttributes.length; i++)
			CreateObject.setAttribute(folder, folderAttributes[i]);
		folder.save(folderRefreshMode);
		return folder;
	}

	private static String getContentType(String fileName) {
		try {
			String ext = fileName.substring(fileName.indexOf('.'));
			if (ext.contains(".txt"))
				return "text/plain";
			else if (ext.contains(".doc"))
				return "application/msword";
			else if (ext.contains(".docx"))
				return "application/msword";
			else if (ext.contains(".jpg"))
				return "image/jpeg";
			else if (ext.contains(".gif"))
				return "image/gif";
			else if (ext.contains(".bmp"))
				return "image/bmp";
			else if (ext.contains(".xls"))
				return "application/excel";
			else if (ext.contains(".xlsx"))
				return "application/excel";
			else if (ext.contains(".pdf"))
				return "application/pdf";
			else if (ext.contains(".png"))
				return "image/png";
			else if (ext.contains(".xfdl"))
				return "application/vnd.xfdl";
			else if (ext.contains(".xfdd"))
				return "application/vnd.xfdl.design";
			else if (ext.contains(".tif"))
				return "image/tiff";
		} catch (Exception e) {
		}
		return "application/octet-stream";
	}

	private static void setAttribute(Document document, Attribute attribute)
			throws Exception {
		if (attribute.getType().equals("Object"))
			document.getProperties().putObjectValue(attribute.getName(),
					attribute.getObjectValue());
		else if (attribute.getType().equals("BinaryList"))
			document.getProperties().putValue(attribute.getName(),
					attribute.getBinaryListValue());
		else if (attribute.getType().equals("boolean"))
			document.getProperties().putValue(attribute.getName(),
					attribute.getBooleanValueb());
		else if (attribute.getType().equals("Boolean"))
			document.getProperties().putValue(attribute.getName(),
					attribute.getBooleanValueB());
		else if (attribute.getType().equals("BooleanList"))
			document.getProperties().putValue(attribute.getName(),
					attribute.getBooleanListValue());
		else if (attribute.getType().equals("byte[]"))
			document.getProperties().putValue(attribute.getName(),
					attribute.getByteValue());
		else if (attribute.getType().equals("Date"))
			document.getProperties().putValue(attribute.getName(),
					attribute.getDateValue());
		else if (attribute.getType().equals("DateTimeList"))
			document.getProperties().putValue(attribute.getName(),
					attribute.getDateTimeListValue());
		else if (attribute.getType().equals("DependentObject"))
			document.getProperties().putValue(attribute.getName(),
					attribute.getDependentObjectListValue());
		else if (attribute.getType().equals("double"))
			document.getProperties().putValue(attribute.getName(),
					attribute.getDoubleValued());
		else if (attribute.getType().equals("Double"))
			document.getProperties().putValue(attribute.getName(),
					attribute.getDoubleValueD());
		else if (attribute.getType().equals("EngineObject"))
			document.getProperties().putValue(attribute.getName(),
					attribute.getEngineObjectValue());
		else if (attribute.getType().equals("Float64List"))
			document.getProperties().putValue(attribute.getName(),
					attribute.getFloat64ListValue());
		else if (attribute.getType().equals("Id"))
			document.getProperties().putValue(attribute.getName(),
					attribute.getiDValue());
		else if (attribute.getType().equals("IdList"))
			document.getProperties().putValue(attribute.getName(),
					attribute.getIdListValue());
		else if (attribute.getType().equals("IndependentObjectSet"))
			document.getProperties().putValue(attribute.getName(),
					attribute.getIndependentObjectSetValue());
		else if (attribute.getType().equals("InputStream"))
			document.getProperties().putValue(attribute.getName(),
					attribute.getInputStreamValue());
		else if (attribute.getType().equals("int"))
			document.getProperties().putValue(attribute.getName(),
					attribute.getIntValue());
		else if (attribute.getType().equals("Integer"))
			document.getProperties().putValue(attribute.getName(),
					attribute.getIntegerValue());
		else if (attribute.getType().equals("Integer32List"))
			document.getProperties().putValue(attribute.getName(),
					attribute.getInteger32ListValue());
		else if (attribute.getType().equals("String"))
			document.getProperties().putValue(attribute.getName(),
					attribute.getStringValue());
		else if (attribute.getType().equals("StringList"))
			document.getProperties().putValue(attribute.getName(),
					attribute.getStringListValue());
		else
			throw new Exception(ErrorBundleKeys.ATTRIBUTEMISSING_STRING);
	}

	private static void setAttribute(Folder folder, Attribute attribute)
			throws Exception {
		if (attribute.getType().equals("Object"))
			folder.getProperties().putObjectValue(attribute.getName(),
					attribute.getObjectValue());
		else if (attribute.getType().equals("BinaryList"))
			folder.getProperties().putValue(attribute.getName(),
					attribute.getBinaryListValue());
		else if (attribute.getType().equals("boolean"))
			folder.getProperties().putValue(attribute.getName(),
					attribute.getBooleanValueb());
		else if (attribute.getType().equals("Boolean"))
			folder.getProperties().putValue(attribute.getName(),
					attribute.getBooleanValueB());
		else if (attribute.getType().equals("BooleanList"))
			folder.getProperties().putValue(attribute.getName(),
					attribute.getBooleanListValue());
		else if (attribute.getType().equals("byte[]"))
			folder.getProperties().putValue(attribute.getName(),
					attribute.getByteValue());
		else if (attribute.getType().equals("Date"))
			folder.getProperties().putValue(attribute.getName(),
					attribute.getDateValue());
		else if (attribute.getType().equals("DateTimeList"))
			folder.getProperties().putValue(attribute.getName(),
					attribute.getDateTimeListValue());
		else if (attribute.getType().equals("DependentObject"))
			folder.getProperties().putValue(attribute.getName(),
					attribute.getDependentObjectListValue());
		else if (attribute.getType().equals("double"))
			folder.getProperties().putValue(attribute.getName(),
					attribute.getDoubleValued());
		else if (attribute.getType().equals("Double"))
			folder.getProperties().putValue(attribute.getName(),
					attribute.getDoubleValueD());
		else if (attribute.getType().equals("EngineObject"))
			folder.getProperties().putValue(attribute.getName(),
					attribute.getEngineObjectValue());
		else if (attribute.getType().equals("Float64List"))
			folder.getProperties().putValue(attribute.getName(),
					attribute.getFloat64ListValue());
		else if (attribute.getType().equals("Id"))
			folder.getProperties().putValue(attribute.getName(),
					attribute.getiDValue());
		else if (attribute.getType().equals("IdList"))
			folder.getProperties().putValue(attribute.getName(),
					attribute.getIdListValue());
		else if (attribute.getType().equals("IndependentObjectSet"))
			folder.getProperties().putValue(attribute.getName(),
					attribute.getIndependentObjectSetValue());
		else if (attribute.getType().equals("InputStream"))
			folder.getProperties().putValue(attribute.getName(),
					attribute.getInputStreamValue());
		else if (attribute.getType().equals("int"))
			folder.getProperties().putValue(attribute.getName(),
					attribute.getIntValue());
		else if (attribute.getType().equals("Integer"))
			folder.getProperties().putValue(attribute.getName(),
					attribute.getIntegerValue());
		else if (attribute.getType().equals("Integer32List"))
			folder.getProperties().putValue(attribute.getName(),
					attribute.getInteger32ListValue());
		else if (attribute.getType().equals("String"))
			folder.getProperties().putValue(attribute.getName(),
					attribute.getStringValue());
		else if (attribute.getType().equals("StringList"))
			folder.getProperties().putValue(attribute.getName(),
					attribute.getStringListValue());
		else
			throw new Exception(ErrorBundleKeys.ATTRIBUTEMISSING_STRING);
	}
}