package com.techHub.services;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.techHub.daos.taskDao;
import com.techHub.factories.DaosFactory;

/**
 * Servlet implementation class Task
 */
@WebServlet({ "/TaskHistory", "/TaskList", "/Search", "/Near", "/TaskDetails", "/ViewOtherBaskets" ,"/ListFilters","/TaskCustomAction"})
public class Task extends HttpServlet {
	DaosFactory daosFactory = new DaosFactory();
	final Gson gson = new Gson();
	Map result= new HashMap<>();
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Task() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		response.getWriter().append("helloooooooooooooo ");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String url= request.getRequestURI().substring(request.getContextPath().length());
		if (url.equalsIgnoreCase("/TaskList"))
		{
			TaskList(request,response);
			
		}
		else if (url.equalsIgnoreCase("/Search"))
		{
			Search(request,response);
		}
		else if (url.equalsIgnoreCase("/TaskHistory"))
		{
			
		}
		else if (url.equalsIgnoreCase("/Near"))
		{
			
		}
		else if (url.equalsIgnoreCase("/TaskDetails"))
		{
			int taskId;
			String token,strTaskId;
			boolean result;
			Map returnedJsonMap=new HashMap<>();
			
			strTaskId=request.getParameter("TaskId");
			token=request.getParameter("Token");
			taskId=Integer.parseInt(strTaskId);
			
			taskDao taskDaoObj=DaosFactory.getTaskDao();
			//call hussien function
			
			result=false;
			
			if(result) {
				returnedJsonMap.put("result",result);
				returnedJsonMap.put("token",token);
				
			}
			
			else {
				returnedJsonMap.put("result",result);
				returnedJsonMap.put("error_message","error!");
			}
			
			String jsonString;
			Gson gson=new Gson();
			jsonString=gson.toJson(returnedJsonMap);
			
			response.getWriter().append(jsonString);
			
			
		}
		else if (url.equalsIgnoreCase("/ViewOtherBaskets"))
		{
			getOtherBasket(request, response);
		}
		else if (url.equalsIgnoreCase("/ListFilters"))
		{
			ListFilters(request, response);
		}
		else if (url.equalsIgnoreCase("/TaskCustomAction"))
		{
			TaskCustomAction(request,response);
		}
		//doGet(request, response);
	}
	private void Search(HttpServletRequest request,HttpServletResponse response) throws IOException {
		taskDao taskdao= daosFactory.getTaskDao();
		PrintWriter out = response.getWriter();
		
		//The needed parameters for the Search method
		String Keyword=request.getParameter("Keyword");
		String Property=request.getParameter("Property");
		
		// Create the return object
		HashMap<Object,Object>map=new HashMap<Object,Object>();
		map.put("result", "true");
		map.put("token", "new token");
		map.put("inbox_tasks", taskdao.searchInbox(Property,Keyword));
		
		// Create a Json Object
	    String result=gson.toJson(map);
	    out.append(result);
	}
	private void getOtherBasket(HttpServletRequest request, HttpServletResponse response) {
		//Needed parameters for the getOtherBasket method
		String appSpaceName; 
		String role; 
		String inBasket;
		
		//user info
		String userId;
		String token;
		
		//getting the data from the request body
		appSpaceName = request.getParameter("appSpaceName");
		role = request.getParameter("role");
		inBasket = request.getParameter("inBasket");
		userId = request.getParameter("UDID");
		token = request.getParameter("Token");
		
		
		//converting the result to jason format 
		String jasonResult = gson.toJson( 
				daosFactory.getTaskDao().getOtherInbox(appSpaceName, role, inBasket)
				);
		
		//writing the jason to the response body 
		try {
			response.getWriter().append(jasonResult);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	void TaskList (HttpServletRequest request, HttpServletResponse response)
	{
		// get the token from the request body 
		String token= request.getParameter("Token");
		// get the device id from the request body 
		String UDID= request.getParameter("UDID");
		//create object from Task dao 
		taskDao taskdao= daosFactory.getTaskDao();
		
		try {
			result.put("result: ", "true");
			result.put("token: ", token);
			// get all tasks from inbox 
			result.put("CaseDetails: ", taskdao.getInbox());
			String resultInJsonFormat=gson.toJson(result);
			//append the result to response 
			response.getWriter().append(resultInJsonFormat);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	void TaskHistory(HttpServletRequest request, HttpServletResponse response) {
		String TaskId=request.getParameter("TaskId");
		// call heussin's function
		// make a json object and return it
	}
	
	
	void ListFilters (HttpServletRequest request, HttpServletResponse response)
	{
		// get the token from the request body 
		String token= request.getParameter("Token");
		
		
		try {
			result.put("result: ", "true");
			result.put("token: ", token);
			// get all tasks from inbox 
			
			/*
			hussien function 
			result.put("CaseDetails: ", taskdao.getInbox());
			
			*/
			String resultInJsonFormat=gson.toJson(result);
			//append the result to response 
			response.getWriter().append(resultInJsonFormat);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	void TaskCustomAction (HttpServletRequest request, HttpServletResponse response)
	{
		// get the token from the request body 
		String token= request.getParameter("Token");
		// get the Task id from the request body 
		int  taskId = Integer.parseInt(request.getParameter("TaskId"));
		// get the Required Action from the request body 
		String RequiredAction= request.getParameter("RequiredAction");
		
		// if true 
		try {
			result.put("result: ", "true");
			result.put("token: ", token);
			// get all tasks from inbox 
			String resultInJsonFormat=gson.toJson(result);
			//append the result to response 
			response.getWriter().append(resultInJsonFormat);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		/*
		 * else 
		 * */
		
		
				
	}
	
	
	

}
