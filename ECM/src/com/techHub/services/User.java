package com.techHub.services;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.techHub.DB.Crud;
import com.techHub.daos.AuthenticationDao;
import com.techHub.daos.ConfigDao;
import com.techHub.daos.TokenDaos;
import com.techHub.daos.UserDaos;
import com.techHub.factories.DBDAOSFactories;
import com.techHub.factories.DaosFactory;
import com.techHub.models.TokenModel;
import com.techHub.models.UserModel;

/**
 * Servlet implementation class User
 */
@WebServlet({ "/User", "/getUserProfile", "/updateUserImage" ,"/login"})
public class User extends HttpServlet {
	Crud c= new Crud();
	final Gson gson = new Gson();
	Connection connection= null;
	Map result= new HashMap<>();
	private static final long serialVersionUID = 1L;

    /**
     * Default constructor. 
     */
    public User() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String url= request.getRequestURI().substring(request.getContextPath().length());
		System.out.println(url);
		if (url.equalsIgnoreCase("/getUserProfile"))
		{
			// get the token from the request body 
			String token= request.getParameter("Token");
			//create object from Task dao 
			TokenDaos dao = new TokenDaos();
			//create object from User dao 
			UserDaos dao2 = new UserDaos();
			//get the user how owns this token 
			UserModel user=dao2.findUserById(dao.getUserId(token));
			// call hussien fun 
			// convert 
			// add to response 
			
			result.put("result: ", "true");
			result.put("token: ", token);
			result.put("user: ", user);
			String  resultInJsonFormat=gson.toJson(result);
			response.getWriter().append(resultInJsonFormat);
		//	System.out.println(user.getUserName());
			
		}
		else if (url.equalsIgnoreCase("/updateUserImage"))
		{
			// get the token from the request body 
			String token= request.getParameter("Token");
			// get the Image data  from the request body 
		//	String image= request.getParameter("base64Image");
			//create object from Task dao 
			TokenDaos dao = new TokenDaos();
			//create object from User dao 
			UserDaos dao2 = new UserDaos();
			//get the user how owns this token 
			UserModel user=dao2.findUserById(dao.getUserId(token));
			 
			result.put("result: ", "true");
			result.put("token: ", token);
			result.put("user: ", user);
			String resultInJsonFormat=gson.toJson(result);
			response.getWriter().append(resultInJsonFormat);
			// call hussien fun 
			// convert 
			// add to response 
			// call husien functin then convert and retuen 
//			String resultInJsonFormat=gson.toJson();
			
		}
		else if (url.equalsIgnoreCase("/login")) {
			String jsonString = null;
			
			ConfigDao cd=new ConfigDao();
			try {
				Map m=cd.getLoginConfi();
				System.out.println(m);
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			System.out.println();
			
			try {
				jsonString = login(request);
			} catch (SQLException | ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			response.getWriter().append(jsonString);
		}

		
		//doGet(request, response);
	}
	public String login(HttpServletRequest request) throws SQLException, ParseException {
		String userName, password,udid,jsonString = null;
		Map returnedJsonMap=new HashMap<>();
		boolean checkLogin;
		int lastInsertedId;
		ConfigDao confDao=DBDAOSFactories.getConfiDao();
		

		
		//get values of request parameter
		
	    userName=request.getParameter("Username");
		password=request.getParameter("Password");
		udid=request.getParameter("UDID");
		
		//get configuration values
		Map m=confDao.getLoginConfi();
		
		String objectStore= m.get("objectStore").toString();
		String connectionPoint= m.get("connectionPoint").toString();
		String serverName=m.get("ip").toString();
		String loginContext=m.get("context").toString();
		String Dmoain=m.get("domain").toString();
		
		
		//CMTOS","p8admin","filenet","ICM_CP1","192.168.20.55:9080","FileNetP8WSI","IBM ECM");
		
		AuthenticationDao AuthDao=DaosFactory.getAuthDao();
		
		//checkLogin=AuthDao.login("CMTOS","p8admin","filenet","ICM_CP1","192.168.20.55:9080","FileNetP8WSI","IBM ECM");
		/*try {
		checkLogin=AuthDao.login(objectStore, userName, password, connectionPoint, serverName, loginContext, Dmoain);
		}catch (Exception e) {
			System.out.println(e.getMessage());
			checkLogin=false;
		}*/
		
		checkLogin=true;
		
	     if(checkLogin) {
	    	 //Generate Token
	    	 String tokenStr=UUID.randomUUID().toString();
	    	 
	    	 java.util.Date date=new java.util.Date();
	    
	         Calendar c=new GregorianCalendar();
	         c.add(Calendar.DATE, 30);
	         date=c.getTime();
	         SimpleDateFormat ft = new SimpleDateFormat ("MM/dd/yyyy");
	         String dateAfterMonth=ft.format(date);
	         
	         
	         //insert new user
	     
	    	 UserModel user=new UserModel(userName, password);
	    	 UserDaos userDao=DBDAOSFactories.getUserDaos();
	    	 lastInsertedId=userDao.insertNewUser(user);
	    	 
	    	 // insert new token for this user
	    	 TokenModel token=new TokenModel(lastInsertedId,dateAfterMonth,tokenStr, udid);
	    	 TokenDaos  tokenDaos=DBDAOSFactories.getTokenDaos();
	    	 tokenDaos.insertNewToken(token);
	    	 
	    	 tokenDaos.isValidToken(tokenStr);
	    	 
	    	 UserModel user2=tokenDaos.getUser(tokenStr);
	    	 System.out.println(user.getUserName());
	    	 System.out.println(user.getPassword());
	    	 
	    	 
	    	 returnedJsonMap.put("result",checkLogin);
	    	 returnedJsonMap.put("token",tokenStr);

	    	 
	     }
	     else  {
	    	 returnedJsonMap.put("result",checkLogin);
	    	 returnedJsonMap.put("error_message","Invalid Email or Password");
	     }
//    	 
    	 Gson gson =new Gson();
    	 jsonString=gson.toJson(returnedJsonMap);
    	// AuthDao.close();
    	 
    	 
	  
	     return jsonString;
		
		
		
    }
	

}
