package com.techHub.services;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.techHub.factories.DaosFactory;

/**
 * Servlet implementation class TaskReport
 */
@WebServlet({ "/TaskReport", "/MyTasksReport", "/AssignedTasksReport", "/CertainUsersTasksReport" })
public class TaskReport extends HttpServlet {
	DaosFactory daosFactory = new DaosFactory();
	final Gson gson = new Gson();
	Map result= new HashMap<>();
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public TaskReport() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String url= request.getRequestURI().substring(request.getContextPath().length());
		if (url.equalsIgnoreCase("/MyTasksReport"))
		{
			MyTasksReport(request, response);
			
		}
		else if (url.equalsIgnoreCase("/AssignedTasksReport"))
		{
			// get the token from the request body 
			String token= request.getParameter("Token");
			// get the Duration in days from the request body 
			int  duration= Integer.parseInt(request.getParameter("DurationInDays"));
		}
		else if (url.equalsIgnoreCase("/CertainUsersTasksReport"))
		{
			
		}
		
		
	}
	
	
	void MyTasksReport(HttpServletRequest request, HttpServletResponse response) 
	{
		// get the token from the request body 
		String token= request.getParameter("Token");
		// get the Duration in days from the request body 
		int  duration= Integer.parseInt(request.getParameter("DurationInDays"));
		
		
		int allTasks =0;
		int done=0;
		int inProgress=0;
		int delayes=0;
		
		result.put("result: ", "true");
		result.put("token: ", token);
		result.put("all_tasks:", allTasks);
		result.put("done:", done);
		result.put("in_progress:", inProgress);
		result.put("delayes:",delayes);
		
		String resultInJsonFormat=gson.toJson(result);
		//append the result to response 
		try {
			response.getWriter().append(resultInJsonFormat);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	void AssignedTasksReport(HttpServletRequest request, HttpServletResponse response) 
	{
		// get the token from the request body 
		String token= request.getParameter("Token");
		// get the Duration in days from the request body 
		int  duration= Integer.parseInt(request.getParameter("DurationInDays"));
		// get the Duration in days from the request body 
		int  userId = Integer.parseInt(request.getParameter("userId"));
		
		
		int allTasks =0;
		int done=0;
		int inProgress=0;
		int delayes=0;
		
		result.put("result: ", "true");
		result.put("token: ", token);
		result.put("all_tasks:", allTasks);
		result.put("done:", done);
		result.put("in_progress:", inProgress);
		result.put("delayes:",delayes);
	}
	void CertainUsersTasksReport(HttpServletRequest request, HttpServletResponse response) 
	{
		// get the token from the request body 
		String token= request.getParameter("Token");
		
		int numberOfDelayedTasks =0;
		String tasks="";
		
		
		result.put("result: ", "true");
		result.put("token: ", token);
		result.put("numberOfDelayedTasks:", numberOfDelayedTasks);
		result.put("Tasks:", tasks);
		
		
	}
	

}
