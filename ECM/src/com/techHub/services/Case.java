package com.techHub.services;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.techHub.daos.caseDao;
import com.techHub.factories.DaosFactory;

/**
 * Servlet implementation class Case
 */
@WebServlet({ "/Case", "/CaseDetails", "/ViewCase" })
public class Case extends HttpServlet {
	DaosFactory daosFactory = new DaosFactory();
	final Gson gson = new Gson();
	Map result= new HashMap<>();
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Case() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		response.getWriter().println("Hello World");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		// get the requseted url then choose fuction to do 
		String url= request.getRequestURI().substring(request.getContextPath().length());
		if (url.equalsIgnoreCase("/CaseDetails"))
		{

			
			getCaseDetails(request,response);
			
			String token= request.getParameter("Token");
			String caseId= request.getParameter("CaseId");
			caseDao caseDao =daosFactory.getCaseDao();
			String resultInJsonFormat=gson.toJson(caseDao.getCase(caseId));
			response.getWriter().append(resultInJsonFormat);

		}
		else if (url.equalsIgnoreCase("/ViewCase"))
		{
			ViewCase(request,response);
		}

		
		
		
		
		//doGet(request, response);
	}
	void getCaseDetails (HttpServletRequest request, HttpServletResponse response)
	{
			// get the token from the request body 
			String token= request.getParameter("Token");
			// get the case id  from the request body   
			String caseId= request.getParameter("CaseId");
			//create object from case dao 
			caseDao caseDao =daosFactory.getCaseDao();
			  
			// add result to the response 
			try {
				result.put("result: ", "true");
				result.put("token: ", token);
				// get case details by send case id
				result.put("CaseDetails: ", caseDao.getCase(caseId));
				String resultInJsonFormat=gson.toJson(result);
				response.getWriter().append(resultInJsonFormat);
			} 
			catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

	}
	private void ViewCase(HttpServletRequest request, HttpServletResponse response) throws IOException {
		//The needed parameters for the Search method
		String TaskId=request.getParameter("TaskId");
		caseDao casedao=daosFactory.getCaseDao();
		
		// create the return object
		HashMap<Object,Object>map=new HashMap<Object,Object>();
		map.put("result", "true");
		map.put("token", "new token");
		map.put("caseDetails", casedao.getCaseUsingTaskID(TaskId));
		
		// Create a Json Object
		String result=gson.toJson(map);
		response.getWriter().append(result);
	}
}
