package com.techHub.DB;


import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.SQLException;

public class DBConnection {
	private static Connection connection = null;
	private final static String  serveIp="192.168.20.55";
	private final static String  DBname="ExtrenalDatabase";
	private final static String  userName="IntAdmin";
	private final static String  password="IntTech@2017";
	
	public static Connection getActiveConnection() {
		String connectionUrl = "jdbc:sqlserver://"+serveIp+";" +
				"databaseName="+DBname+";user="+userName+";password="+password+";";
			
	        	try {
	        		Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
//	        		Class.forName("com.microsoft.jdbc.sqlserver.SQLServerDriver");
	        		connection = DriverManager.getConnection(connectionUrl);
	        		return connection;
	        	}
			catch (Exception e) {
				e.printStackTrace();
			}
	        	return null;
	}
	
	
	public static boolean   closeConnection ()
	{
		try {
			connection.close();
			return true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
		
	}
}