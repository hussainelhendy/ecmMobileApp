package com.techHub.models;


public class UserModel {
	private int userId ;
	private String userName;
	private String password;
	
	public int getuserId() {
		return userId;
	}
	
	
	


	public void setUserId(int userId) {
		this.userId = userId;
	}


	public UserModel(String userName, String password) {
		super();
		
		this.userName = userName;
		this.password = password;
	}

	

	public UserModel() {
		super();
	}



	public String getUserName() {
		return userName;
	}
	public void  setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	
	

}
