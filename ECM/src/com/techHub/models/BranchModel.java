package com.techHub.models;


public class BranchModel {
	private int branchId;
	private String branchName;
	private LocationModel location;
	
	public BranchModel() {}
	
	public BranchModel(int branchId, String branchName, LocationModel location) {
		this.branchId = branchId;
		this.branchName = branchName;
		this.location = location;
	}

	public int getBranchId() {
		return branchId;
	}

	public void setBranchId(int branchId) {
		this.branchId = branchId;
	}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public LocationModel getLocation() {
		return location;
	}

	public void setLocation(LocationModel location) {
		this.location = location;
	}

}
