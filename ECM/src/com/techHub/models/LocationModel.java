package com.techHub.models;

public class LocationModel {
	private int locationId;
	private double lat;
	private double lon;
	


	public LocationModel(double lat, double lon,int locationId) {
		super();
		this.locationId=locationId;
		this.lat = lat;
		this.lon = lon;
	}


	public int getLocationId() {
		return locationId;
	}


	public void setLocationId(int locationId) {
		this.locationId = locationId;
	}


	public double getLat() {
		return lat;
	}


	public void setLat(double lat) {
		this.lat = lat;
	}


	public double getLon() {
		return lon;
	}


	public void setLon(double lon) {
		this.lon = lon;
	}
	
	
	
	
	


}
