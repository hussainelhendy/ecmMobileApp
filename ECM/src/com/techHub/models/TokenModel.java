package com.techHub.models;

import java.sql.Date;

public class TokenModel {
	private int UserID;
	private String  ExpirationDate;
	private String token;
	private String DeviceID;
	
	//Constructors
	//------------
	public TokenModel(){			//Default
		UserID =0;
		ExpirationDate = null;
		token = null;		
	}
	
	public TokenModel(int UserId, String ExpDate, String Token,String DeviceID){		//Parameterized
		UserID = UserId;
		ExpirationDate = ExpDate;
		token = Token;
		this.DeviceID=DeviceID;
	}
	
	
	

	//Setters and Getters
	//-------------------
	public int getUserId() {
		return UserID;
	}

	public void setUserId(int userId) {
		this.UserID = userId;
	}

	public String getExpirationDate() {
		return ExpirationDate;
	}

	public void setExpirationDate(String expirationDate) {
		ExpirationDate = expirationDate;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getDeviceID() {
		return DeviceID;
	}

	public void setDeviceID(String deviceID) {
		DeviceID = deviceID;
	}	
}

