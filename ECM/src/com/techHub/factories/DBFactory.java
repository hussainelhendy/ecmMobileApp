package com.techHub.factories;

import com.techHub.models.*;

public class DBFactory {
	public UserModel getUser() {
		return new UserModel();
	}
	public BranchModel getBranch() {
		return new BranchModel();
	}
	public TokenModel getToken() {
		return new TokenModel();
	}
}
