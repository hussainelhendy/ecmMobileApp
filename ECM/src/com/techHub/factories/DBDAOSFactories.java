package com.techHub.factories;

import com.techHub.daos.ConfigDao;
import com.techHub.daos.TokenDaos;
import com.techHub.daos.UserDaos;


public class DBDAOSFactories {
	public static UserDaos getUserDaos() {
		return new UserDaos();
	}
	
	public static TokenDaos getTokenDaos() {
		return new TokenDaos();
	}
	public static ConfigDao getConfiDao() {
		return new ConfigDao();
	}

}
