package com.techHub.daos;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.techHub.DB.Crud;
import com.techHub.DB.DBConnection;
import com.techHub.DB.Pair;
import com.techHub.factories.DBDAOSFactories;
import com.techHub.factories.DaosFactory;
import com.techHub.models.UserModel;

public class UserDaos {

	public UserModel findUserById(int id ) {
		
	

	Connection connection = DBConnection.getActiveConnection();
		Crud c= new Crud ();
		UserModel user = null;
		ResultSet st=c.select("UserModel","userId",id+"",connection);
		try {
			if (st.next())
			{
				 user = new UserModel();
				user.setPassword(st.getString("password"));
				user.setUserName(st.getString("userName"));
				return user;
			}
			else System.out.println("not found ");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		DBConnection.closeConnection();
		return user;
	}
	public int insertNewUser(UserModel user) throws SQLException {
		
		Connection conn=DBConnection.getActiveConnection();
		ResultSet rs;
		int Last_inserted_id = 0;
		
		Crud c = new Crud();
		ArrayList<Pair> pairs=new ArrayList<>();
		pairs.add(new Pair("userName",user.getUserName()));
		pairs.add(new Pair("password",user.getPassword()));
		c.insertRecord("UserModel", pairs);
		rs=c.select("UserModel","userName",user.getUserName(), conn);

		
		while(rs.next()) {
			Last_inserted_id=rs.getInt("userId");
		}
		
		DBConnection.closeConnection();

		return Last_inserted_id;
		
	}
	public boolean Login(String token) throws SQLException {
		
		TokenDaos tokenDaos=DBDAOSFactories.getTokenDaos();
		boolean checkLogin;
		UserModel user=tokenDaos.getUser(token);
		String username=user.getUserName();
		String password=user.getPassword();
		Map m=new HashMap<>();
		ConfigDao confDao=DBDAOSFactories.getConfiDao();
		
		String objectStore= m.get("objectStore").toString();
		String connectionPoint= m.get("connectionPoint").toString();
		String serverName=m.get("ip").toString();
		String loginContext=m.get("context").toString();
		String Dmoain=m.get("domain").toString();
		
		AuthenticationDao AuthDao=DaosFactory.getAuthDao();
		
		checkLogin=AuthDao.login(objectStore,username,password,connectionPoint, serverName, loginContext, Dmoain);
		
		AuthDao.close();
		
		return checkLogin;
	}
	
	
}
