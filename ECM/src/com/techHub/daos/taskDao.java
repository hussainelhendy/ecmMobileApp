package com.techHub.daos;

import java.util.ArrayList;

import com.techHub.models.taskModel;
import com.techHub.wrappers.api.CaseManagerAPIWrapper;



public class taskDao {
	
	public taskDao() {
		// TODO Auto-generated constructor stub
	}
	
	public ArrayList<taskModel> getInbox(CaseManagerAPIWrapper apiWrapper) {
		return apiWrapper.getInbox();
	}

	public ArrayList<taskModel> getOtherInbox(CaseManagerAPIWrapper apiWrapper,String appSpaceName, String Role, String inbasket) {
		return apiWrapper.getOtherInbox(appSpaceName, Role, inbasket);
	}

	public ArrayList<taskModel> searchInbox(CaseManagerAPIWrapper apiWrapper,String Property,String KeyWord) {
		return apiWrapper.searchInbox(Property, KeyWord);
	}

	

}
