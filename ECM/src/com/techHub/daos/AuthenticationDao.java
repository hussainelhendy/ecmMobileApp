package com.techHub.daos;

import com.techHub.wrappers.api.CaseManagerAPIWrapper;

public class AuthenticationDao {
	
	CaseManagerAPIWrapper apiWrapper = new CaseManagerAPIWrapper();
	public boolean login(String objectStore,String userName, String password, String connectionPoint, String serverName,String loginContext,String Dmoain) {
		try {
		return apiWrapper.login(objectStore, userName, password, connectionPoint, serverName, loginContext, Dmoain);
		}catch (Exception e) {
			return false;
		}
	}
	public void close() {
		//CaseManagerAPIWrapper cm=new CaseManagerAPIWrapper();
		apiWrapper.close();
	
	}

}
