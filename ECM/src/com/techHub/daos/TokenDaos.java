package com.techHub.daos;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import com.techHub.DB.Crud;
import com.techHub.DB.DBConnection;
import com.techHub.DB.Pair;
import com.techHub.factories.DBDAOSFactories;
import com.techHub.models.TokenModel;
import com.techHub.models.UserModel;

public class TokenDaos {
public int getUserId (String token)
{
	Connection connection = DBConnection.getActiveConnection();
	Crud c= new Crud ();
	int id=-1;
	ResultSet st=c.select("Token","token_String",token,connection);
	try {
		if (st.next())
		{
			TokenModel tokenModel = new TokenModel();
			tokenModel.setToken(token);
			tokenModel.setExpirationDate(st.getString("expire_Date"));
			tokenModel.setUserId(id=st.getInt("userId"));
			tokenModel.setDeviceID(st.getString("diviceId"));

		}
		else System.out.println("not found ");
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	DBConnection.closeConnection();
	return id;

}
public boolean insertNewToken(TokenModel token) {
	  //Make sure names are correct
	  Crud c = new Crud();
	  boolean b=true;
	  
	  ArrayList<Pair> pairs=new ArrayList<>();
	  
	  pairs.add(new Pair("userId",token.getUserId()+""));
	  pairs.add(new Pair("token_String",token.getToken()));
	  pairs.add(new Pair("expire_Date",token.getExpirationDate()));
	  pairs.add(new Pair("diviceId",token.getDeviceID()));
	  
	  b=c.insertRecord("Token",pairs);
    
		//DBConnection.closeConnection();
		
		return b;
	  
	}
	public boolean isValidToken(String token) throws SQLException, ParseException {
		Crud c = new Crud();
		ResultSet rs;
		String [] date,date2;
		Connection conn=DBConnection.getActiveConnection();
		rs=c.select("Token","token_String",token, conn);
		
		if(rs.next()) {
			String expDate=rs.getString("expire_Date");
			date=expDate.split("-");
			 
			 Date dateNow=new Date();
	         Calendar ca=new GregorianCalendar();
	         dateNow=ca.getTime();
	         SimpleDateFormat ft = new SimpleDateFormat ("yyyy-MM-dd");
	         String dateNowStr=ft.format(dateNow);
	         date2=dateNowStr.split("-");
	         
			Date expDateObj=new Date(Integer.parseInt(date[0]),Integer.parseInt(date[1]),Integer.parseInt(date[2]));
			Date dateNowObj=new Date(Integer.parseInt(date2[0]),Integer.parseInt(date2[1]),Integer.parseInt(date2[2]));
			//Date expDateObj=new Date(2010,2,2);

			boolean isValidToken=expDateObj.after(dateNowObj);
			System.out.println(isValidToken);
			
			return isValidToken;
			
		}
		else {
			System.out.println("No token");
			return false;
		}		
	}
	
public UserModel getUser(String token) throws SQLException {
	UserModel user=new UserModel();
	Connection conn=DBConnection.getActiveConnection();
	ResultSet rs;
	Crud c=new Crud();
	int userId=getUserId(token);
	if(userId!=0) {
		rs=c.select("UserModel","userId",userId+"",conn);
		
		if(rs.next()) {
			String userName=rs.getString("userName");
			String password=rs.getString("password");
			user.setUserName(userName);
			user.setPassword(password);	
			user.setUserId(userId);
			
			return user;
		}
		else {
			return null;
		}
		
	}
	else {
		return null;
	}
		
}
public static void main(String[] args) {
	DBDAOSFactories dbdaosFactories= new DBDAOSFactories();
	TokenDaos dao =dbdaosFactories.getTokenDaos();
	UserDaos dao2 = dbdaosFactories.getUserDaos();
	// send to Hussien and return ;
	System.out.println(dao2.findUserById(dao.getUserId("newone")).getUserName());
}

}
