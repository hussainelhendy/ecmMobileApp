package com.techHub.daos;

import com.techHub.models.CaseModel;
import com.techHub.wrappers.api.CaseManagerAPIWrapper;

public class caseDao {

	public caseDao() {
		// TODO Auto-generated constructor stub
	}
	
	public CaseModel getCase(String caseID){
		return new CaseManagerAPIWrapper().getCase(caseID);
	}
	
	public CaseModel getCaseUsingTaskID(String taskID){
		return new CaseManagerAPIWrapper().getCaseUsingTaskID(taskID);
	}
	
	public CaseModel getCases(String classType){
		return new CaseManagerAPIWrapper().getCases(classType);
	}
}
