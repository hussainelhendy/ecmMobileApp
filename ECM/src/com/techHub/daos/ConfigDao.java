package com.techHub.daos;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import com.techHub.DB.Crud;
import com.techHub.DB.DBConnection;

public class ConfigDao {
	
	public Map getLoginConfi() throws SQLException {
		Map m=new HashMap<>();
		Connection conn=DBConnection.getActiveConnection();
		Crud c = new Crud();
		ResultSet rs;
		rs=c.select("configurationModel", conn);
		
		while (rs.next()) {
			String key=rs.getString("config_key");
			String value=rs.getString("value");
			m.put(key, value);
		}
		
		return m;
	}

}
